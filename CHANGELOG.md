# Release Notes

## 0.2.0 (2024-07-02)
- Added NLG Observer which allows simple nlg and also calling external modules for complex nlg
- Implemented 2 default NLGRequests `Say` (just verbalize the defined text) and `Fill` (select and fill a template)
- fix typo name in `observer_class` variable in `ObserverReq` class

## 0.1.0 (2023-11-02)
- Created