# Simple Flexdiam

[API Docs](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos_pkg_simple_flexdiam)

A dialog manager for aaambos based on the ideas from flexdiam (a flexible dialog manager) but in its current state a reduced and simplified version

## Install

```bash
conda activate aaambos
git clone https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos_pkg_simple_flexdiam.git
cd aaambos_pkg_simple_flexdiam
pip install -e .
```

## Running

Example `arch_config` part:
```yaml
modules:
  simple_flexdiam:
    module_info: !name:simple_flexdiam.modules.simple_flexdiam_module.SimpleFlexdiam
    flow_handler:
      flow_handler_class: !name:simple_flexdiam.modules.issue_flow_handler.issue_flow_handler.IssueFlowHandler
      root_issue: !name:simple_flexdiam.modules.std.issues.root_issue.RootIssue
      shadow_root_issues:
        - !name:simple_flexdiam.modules.std.issues.greet_issue.GreetIssue
        - !name:simple_flexdiam.modules.std.issues.how_are_you_issue.HowAreYouIssue
        - !name:simple_flexdiam.modules.std.issues.repeat_issue.RepeatIssue
      entry_point_finder: !name:simple_flexdiam.modules.issue_flow_handler.simple_entry_point_finder.SimpleEntryPointFinder
```
