#!/usr/bin/env python

"""The setup script."""

from setuptools import setup, find_packages

with open('README.md') as readme_file:
    readme = readme_file.read()

with open('CHANGELOG.md') as history_file:
    history = history_file.read()

requirements = ["aaambos @ git+https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos@main"]

test_requirements = ['pytest>=3', "pytest-timeout>=2.3.1", "pytest-cov>=6.0.0"]

setup(
    author="Florian Schröder",
    author_email='florian.schroeder@uni-bielefeld.de',
    python_requires='>=3.10',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.10',
    ],
    description="A dialog manager for aaambos based on the ideas from flexdiam (a flexible dialog manager) but in its current state a reduced and simplified version",
    entry_points={
        'aaambos.core.create': [
            'issue = simple_flexdiam.plugins.create:issue',
            'observer = simple_flexdiam.plugins.create:observer',
            'tier = simple_flexdiam.plugins.create:tier',
            'xmit = simple_flexdiam.plugins.create:xmit',
            #'entry_point_finder = simple_flexdiam.plugins.create:entry_point_finder',
            #'flow_handler = simple_flexdiam.plugins.create:flow_handler',
        ],
    },
    install_requires=requirements,
    license="MIT license",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords=['aaambos', 'simple_flexdiam', 'dialog_manager', 'dialog', 'flexdiam'],
    name='simple_flexdiam',
    packages=find_packages(include=['simple_flexdiam', 'simple_flexdiam.*']),
    test_suite='simple_flexdiaum/tests',
    extras_require={"test": test_requirements},
    url='https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos_pkg_simple_flexdiam',
    version='0.3.1',
    zip_safe=False,
)