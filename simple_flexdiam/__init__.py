"""

This is the documentation of the aaambos package Simple Flexdiam.
It contains of aaambos modules, owm run and/or arch configs,

# About the package
It implements a simple version of the dialog manager Flexdiam.
Based on Natural Language Understanding results it produces Speech Generation outputs.
But its behavior is completely customizable with custom observers and flow handler.
At the moment, the IssueFlowHandler is implemented. You can specify the root issue and the shadow issues to add to the root issue.

Shadow issues are triggered when suitable entry points are received, e.g., the correct intention detected by the NLU.
Issues can add child issues when they are active. The handle_entry_point methods and handle_prompt_request are called.
handle_prompt_request is similar to a step or update function but is only called if the issue is active and is the chosen by the flow handler.

# Background / Literature
For the original Flexdiam, see
- [flexdiam – flexible dialogue management for problem-aware, incremental spoken interaction for all user groups](https://pub.uni-bielefeld.de/record/2906208)
- [flexdiam – flexible dialogue management for incremental interaction with virtual agents](https://pub.uni-bielefeld.de/record/2906209)

# Usage / Examples
Add the simple flexdiam module to your `modules` in your arch_config:
```yaml
    flow_handler:
      flow_handler_class: !name:simple_flexdiam.modules.issue_flow_handler.issue_flow_handler.IssueFlowHandler
      root_issue: !name:simple_flexdiam.modules.std.issues.root_issue.RootIssue
      shadow_root_issues:
        - !name:simple_flexdiam.modules.std.issues.greet_issue.GreetIssue
        - !name:simple_flexdiam.modules.std.issues.repeat_issue.RepeatIssue
      entry_point_finder: !name:simple_flexdiam.modules.issue_flow_handler.simple_entry_point_finder.SimpleEntryPointFinder
```

You can set the flow_handler and the root issue. For the beginning, you can use the standard IssueFlowHandler and the root issue.
If you want to use a more complex entry_pont_finder you can change them here.

You can write new Issues and add them to the shadow_root_issues. (Important: You only need to mention the issues that should be tiggered directly from new entry points. Subchildren are handled by the `potential_children_classes` method in each issue.)

A simple issue can look like:

```python
import uuid
from datetime import timedelta
from typing import Type

from simple_flexdiam.modules.core.definitions import SpeechGenerationControl, Controls
from simple_flexdiam.modules.issue_flow_handler.issue import Issue, IssueOutput, IssueState
from simple_flexdiam.modules.issue_flow_handler.issue_flow_handler import Context
from simple_flexdiam.modules.issue_flow_handler.xmit import XMit
from simple_flexdiam.modules.simple_flexdiam_module import SimpleFlexdiamConfig
from simple_flexdiam.modules.std.observers.nlu_entry_point_observer import NLU_PARSE_ENTRY_POINT
from simple_flexdiam.modules.std.xmits.speech_generation_xmit import SpeechGenerationXMit


class GreetIssue(Issue):
    @classmethod
    def get_init_shadow_children(cls, config: SimpleFlexdiamConfig) -> list[Type[Issue] | tuple[Type[Issue], dict]]:
        return []

    @classmethod
    def potential_children_classes(cls, config: SimpleFlexdiamConfig) -> list[Type[Issue]]:
        return []

    @classmethod
    def entry_points_of_interest(cls) -> list[str]:
        return [NLU_PARSE_ENTRY_POINT]

    @classmethod
    def potential_xmit_classes(cls) -> list[Type[XMit]]:
        return []

    def handle_child_closed(self, child, context, output):
        pass

    def can_handle_entry_point(self, entry_point, ep_data, context) -> bool:
        return entry_point == NLU_PARSE_ENTRY_POINT and ep_data.intention in ["greet", "subtlegreet"]

    def handle_entry_point(self, entry_point, ep_data, context: Context, output: IssueOutput):
        if self.can_handle_entry_point(entry_point, ep_data, context):  # should always be True
            speech_gen = SpeechGenerationControl(
                control=Controls.START,
                id=uuid.uuid4(),
                text="Guten Tag!",
            )
            self.locals.greet_utterance = ep_data.best
            xmit = SpeechGenerationXMit(speech_generation=speech_gen, floor_yield=timedelta(seconds=1))
            output.add_xmit(xmit)
            self.set_end_state(IssueState.FULFILLED)

    def handle_prompt_request(self, context, output):
        pass
```

A more complex issue that has a child issue for a yes no question:
```python
class HowAreYouIssue(Issue):
    "Short dialog about, how are you. Trigger it with wie geht es dir."

    def initialize(self):
        self.locals.asked = False

    @classmethod
    def get_init_shadow_children(cls, config: SimpleFlexdiamConfig) -> list[Type[Issue] | tuple[Type[Issue], dict]]:
        pass

    @classmethod
    def potential_children_classes(cls, config: SimpleFlexdiamConfig) -> list[Type[Issue]]:
        return [YNQIssue]

    @classmethod
    def entry_points_of_interest(cls) -> list[str]:
        return [NLU_PARSE_ENTRY_POINT]

    @classmethod
    def potential_xmit_classes(cls) -> list[Type[XMit]]:
        return [SpeechGenerationXMit]

    def handle_child_closed(self, child, context, output):
        if child.abstract_name == "needhelp":
            if child.locals.accepted:
                self.say("Vielleicht hilft es dir wenn du einen guten Freund anrufst. Über Probleme zu reden, ist ein guter Weg sie aus der Welt zu schaffen.", False, output)
                self.set_end_state(IssueState.FULFILLED)
            else:
                self.say("Ok.", floor_yield=False, output=output)
                self.set_end_state(IssueState.FULFILLED)

    def can_handle_entry_point(self, entry_point, ep_data, context) -> bool:
        return entry_point == NLU_PARSE_ENTRY_POINT and (ep_data.intention in ["howareyou"] or (self.locals.asked and ep_data.intention in ["feelingwell", "feelingnotsowell"]))

    def handle_entry_point(self, entry_point, ep_data, context: Context, output: IssueOutput):
        if self.can_handle_entry_point(entry_point, ep_data, context):
            if ep_data.intention == "howareyou":
                self.say("Wenn ich dich sehe, geht es mir eigentlich immer gut! Und wie geht es dir?", 5.0, output)
                self.locals.asked = True
            if ep_data.intention == "feelingwell":
                self.say("Oh, das freut mich!", 5.0, output)
                self.set_end_state(IssueState.FULFILLED)
            if ep_data.intention == "feelingnotsowell":
                self.say("Das klingt aber nicht so gut.", False, output)
                output.add_child(YNQIssue, "needhelp", config={"question": "Willst du darueber reden?"})

    def handle_prompt_request(self, context, output):
        pass

    def say(self, text, floor_yield, output):
        speech_gen = SpeechGenerationControl(
            control=Controls.START,
            id=uuid.uuid4(),
            text=text,
        )
        xmit = SpeechGenerationXMit(speech_generation=speech_gen, floor_yield=timedelta(seconds=floor_yield))
        output.add_xmit(xmit)
```


New possible entry points are received via tiers and handled by observers.
An entry point is first checked against the issue, if it can handle it (can_handle_entry_point`).
Afterward, the `handle_entry_point` method is called.
In it, you can add data to the output (the variable).


# Citation
Because it is based on the original flexdiam, please consider citing it
```
@InProceedings{10.1007/978-3-319-47665-0_64,
author="Yaghoubzadeh, Ramin
and Kopp, Stefan",
editor="Traum, David
and Swartout, William
and Khooshabeh, Peter
and Kopp, Stefan
and Scherer, Stefan
and Leuski, Anton",
title="flexdiam -- Flexible Dialogue Management for Incremental Interaction with Virtual Agents (Demo Paper)",
booktitle="Intelligent Virtual Agents",
year="2016",
publisher="Springer International Publishing",
address="Cham",
pages="505--508",
abstract="We present a demonstration system for incremental spoken human--machine dialogue for task-centric domains that includes a controller for verbal and nonverbal behavior for virtual agents. The dialogue management components can handle uncertainty in input and resolve it interactively with high responsivity, and state tracking is aware of momentary events such as interruptions by the user. Aside from adaptable dialogue strategies, such as for grounding, the system includes a multimodal floor management controller that attempts to limit the influence of idiosyncratic dialogue behavior on the part of our primary user groups -- older adults and people with cognitive impairments -- both of which have previously participated in pilot studies using the platform.",
isbn="978-3-319-47665-0"
}
```

"""
