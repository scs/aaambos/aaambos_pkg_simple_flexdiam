"""
The SimpleFlexdiamModule consists of several parts.

The core folder contains definitions / base classes that are used to write specific implementations.

The std folder contains some example and standard implementations.

There are two flow handler planned that are shipped with simple flexdiam. At the moment, only the issue flow handler is implemented

"""