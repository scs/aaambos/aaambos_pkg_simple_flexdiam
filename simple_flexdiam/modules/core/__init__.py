"""
A SimpleFlexdiam has several "tiers" that contains events (point and interval events). These events are managed by observers.
The dialog flow (or dialog propagation) is done with flow handler which can call methods on observers and handle entry points (triggered by observers).

The organisation of which observer needs which tier and which observer are requested by the flow handler is done automatically with ObserverReq and TierReq (Req: Requests)
"""