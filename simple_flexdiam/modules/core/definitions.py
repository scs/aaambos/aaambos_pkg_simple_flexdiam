
from typing import Any
from datetime import datetime
from enum import Enum

from msgspec import Struct, field as m_field
from aaambos.std.communications.utils import create_promise_and_unit, create_in_out_com_feature
from aaambos.std.communications.categories import MSGSPEC_STRUCT_TYPE

ASR_RESULTS = "AsrResults"


class AsrResultState(Enum):
    HYPOTHESIS = "hypothesis"
    FINAL = "final"


class AsrResults(Struct):
    """AsrResults datastructure"""
    state: AsrResultState
    source: str
    hypotheses: list[str]
    confidences: list[float]
    best: str
    best_confidence: float
    utterance_id: str
    time: datetime = m_field(default_factory=lambda: datetime.now())


AsrResultsPromise, AsrResultsUnit = create_promise_and_unit(
    name=ASR_RESULTS,
    payload_wrapper=AsrResults,
    unit_description="__description of the content of the msg__",
    frequency=1,
    version_str="0.1.0",
    required_service_categories=[{MSGSPEC_STRUCT_TYPE}],
)

AsrResultsComFeatureIn, AsrResultsComFeatureOut = create_in_out_com_feature(
    name=ASR_RESULTS, promise=AsrResultsPromise, version_str="0.1.0", requirements=[], version_compatibility_str=">=0.1.0"
)

# # add the features to your modules
# # modules that send the feature (provide out-feature (and require that some module provides the in-feature)):
# # inside the dict of the provides_features method:
# AsrResultsComFeatureOut.name: (AsrResultsComFeatureOut, SimpleFeatureNecessity.Required),
# # inside the dict of the requires_features method:
# AsrResultsComFeatureIn.name: (AsrResultsComFeatureIn, SimpleFeatureNecessity.Required)

# # modules that receive the feature (provide in-feature (and require that some module provides the out-feature)):
# # inside the dict of the provides_features method:
# AsrResultsComFeatureIn.name: (AsrResultsComFeatureIn, SimpleFeatureNecessity.Required)
# # inside the dict of the requires_features method:
# AsrResultsComFeatureOut.name: (AsrResultsComFeatureOut, SimpleFeatureNecessity.Required),
# # imports: from aaambos.core.module.feature import FeatureNecessity, Feature, SimpleFeatureNecessity
# # and the AsrResultsComFeatureOut and AsrResultsComFeatureIn based on their location.

NLU_RESULTS = "NluResults"


class NluEntity(Struct):
    """The entities detected by the NLU, like name, birthday, etc."""
    entity: str
    value: Any
    start: int
    end: int
    confidence: None | float = None
    extractor: str | None = None


class NluResults(Struct):
    """NluResults datastructure"""
    state: AsrResultState
    intention: str
    utterance: str
    entities: list[NluEntity]
    tokens: list
    noun_phrases: list
    utterance_id: str
    time: datetime = m_field(default_factory=lambda: datetime.now())


NluResultsPromise, NluResultsUnit = create_promise_and_unit(
    name=NLU_RESULTS,
    payload_wrapper=NluResults,
    unit_description="__description of the content of the msg__",
    frequency=1,
    version_str="0.1.0",
    required_service_categories=[{MSGSPEC_STRUCT_TYPE}],
)

NluResultsComFeatureIn, NluResultsComFeatureOut = create_in_out_com_feature(
    name=NLU_RESULTS, promise=NluResultsPromise, version_str="0.1.0", requirements=[], version_compatibility_str=">=0.1.0"
)

# # add the features to your modules
# # modules that send the feature (provide out-feature (and require that some module provides the in-feature)):
# # inside the dict of the provides_features method:
# NluResultsComFeatureOut.name: (NluResultsComFeatureOut, SimpleFeatureNecessity.Required),
# # inside the dict of the requires_features method:
# NluResultsComFeatureIn.name: (NluResultsComFeatureIn, SimpleFeatureNecessity.Required)

# # modules that receive the feature (provide in-feature (and require that some module provides the out-feature)):
# # inside the dict of the provides_features method:
# NluResultsComFeatureIn.name: (NluResultsComFeatureIn, SimpleFeatureNecessity.Required)
# # inside the dict of the requires_features method:
# NluResultsComFeatureOut.name: (NluResultsComFeatureOut, SimpleFeatureNecessity.Required),
# # imports: from aaambos.core.module.feature import FeatureNecessity, Feature, SimpleFeatureNecessity
# # and the NluResultsComFeatureOut and NluResultsComFeatureIn based on their location.

SPEECH_GENERATION_CONTROL = "SpeechGenerationControl"


class Controls(Enum):
    START = "start"
    STOP = "stop"
    SETTINGS = "settings"


class SpeechGenerationControl(Struct):
    """SpeechGenerationControl datastructure"""
    control: Controls
    id: str
    text: str
    options: dict = {}  # todo change and test to m_field(default_factory=dict)
    time: datetime = m_field(default_factory=lambda: datetime.now())


SpeechGenerationControlPromise, SpeechGenerationControlUnit = create_promise_and_unit(
    name=SPEECH_GENERATION_CONTROL,
    payload_wrapper=SpeechGenerationControl,
    unit_description="__description of the content of the msg__",
    frequency=1,
    version_str="0.1.0",
    required_service_categories=[{MSGSPEC_STRUCT_TYPE}],
)

SpeechGenerationControlComFeatureIn, SpeechGenerationControlComFeatureOut = create_in_out_com_feature(
    name=SPEECH_GENERATION_CONTROL, promise=SpeechGenerationControlPromise, version_str="0.1.0", requirements=[], version_compatibility_str=">=0.1.0"
)

# # add the features to your modules
# # modules that send the feature (provide out-feature (and require that some module provides the in-feature)):
# # inside the dict of the provides_features method:
# SpeechGenerationControlComFeatureOut.name: (SpeechGenerationControlComFeatureOut, SimpleFeatureNecessity.Required),
# # inside the dict of the requires_features method:
# SpeechGenerationControlComFeatureIn.name: (SpeechGenerationControlComFeatureIn, SimpleFeatureNecessity.Required)

# # modules that receive the feature (provide in-feature (and require that some module provides the out-feature)):
# # inside the dict of the provides_features method:
# SpeechGenerationControlComFeatureIn.name: (SpeechGenerationControlComFeatureIn, SimpleFeatureNecessity.Required)
# # inside the dict of the requires_features method:
# SpeechGenerationControlComFeatureOut.name: (SpeechGenerationControlComFeatureOut, SimpleFeatureNecessity.Required),
# # imports: from aaambos.core.module.feature import FeatureNecessity, Feature, SimpleFeatureNecessity
# # and the SpeechGenerationControlComFeatureOut and SpeechGenerationControlComFeatureIn based on their location.

SPEECH_GENERATION_STATUS = "SpeechGenerationStatus"


class Status(Enum):
    started = "started"
    running = "running"
    finished = "finished"
    failed = "failed"

    def is_termination_status(self):
        return self in [self.finished, self.failed]


class SpeechGenerationStatus(Struct):
    """SpeechGenerationStatus datastructure"""
    id: str
    status: Status
    time: datetime = m_field(default_factory=lambda: datetime.now())


SpeechGenerationStatusPromise, SpeechGenerationStatusUnit = create_promise_and_unit(
    name=SPEECH_GENERATION_STATUS,
    payload_wrapper=SpeechGenerationStatus,
    unit_description="__description of the content of the msg__",
    frequency=1,
    version_str="0.1.0",
    required_service_categories=[{MSGSPEC_STRUCT_TYPE}],
)

SpeechGenerationStatusComFeatureIn, SpeechGenerationStatusComFeatureOut = create_in_out_com_feature(
    name=SPEECH_GENERATION_STATUS, promise=SpeechGenerationStatusPromise, version_str="0.1.0", requirements=[], version_compatibility_str=">=0.1.0"
)

# # add the features to your modules
# # modules that send the feature (provide out-feature (and require that some module provides the in-feature)):
# # inside the dict of the provides_features method:
# SpeechGenerationStatusComFeatureOut.name: (SpeechGenerationStatusComFeatureOut, SimpleFeatureNecessity.Required),
# # inside the dict of the requires_features method:
# SpeechGenerationStatusComFeatureIn.name: (SpeechGenerationStatusComFeatureIn, SimpleFeatureNecessity.Required)

# # modules that receive the feature (provide in-feature (and require that some module provides the out-feature)):
# # inside the dict of the provides_features method:
# SpeechGenerationStatusComFeatureIn.name: (SpeechGenerationStatusComFeatureIn, SimpleFeatureNecessity.Required)
# # inside the dict of the requires_features method:
# SpeechGenerationStatusComFeatureOut.name: (SpeechGenerationStatusComFeatureOut, SimpleFeatureNecessity.Required),
# # imports: from aaambos.core.module.feature import FeatureNecessity, Feature, SimpleFeatureNecessity
# # and the SpeechGenerationStatusComFeatureOut and SpeechGenerationStatusComFeatureIn based on their location.
