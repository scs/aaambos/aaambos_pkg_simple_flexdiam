from typing import Type

from aaambos.core.module.feature import Feature, CommunicationFeature
from attrs import define, field

from simple_flexdiam.modules.core.observer import Observer


@define(kw_only=True)
class TierReq:
    tier_name: str
    com_in_features: list[CommunicationFeature]
    com_out_features: list[CommunicationFeature]
    is_util: bool = False
    additional_requirements: list[Feature] = field(factory=list)

    def __hash__(self):
        return self.tier_name.__hash__()


@define(kw_only=True)
class ObserverReq:
    observer_class: Type[Observer]
    observer_config: dict


def add_req_to_reqs(req: ObserverReq, reqs: list[ObserverReq]) -> ObserverReq | None:
    for r in reqs:
        if req is r:
            return
        if r.observer_class is req.observer_class:
            if r.observer_config == req.observer_config:
                return
            # deep update?
            r.observer_config.update(req.observer_config)
            return
    reqs.append(req)
    return req


@define(kw_only=True)
class DialogFeature(Feature):
    ...


@define(kw_only=True)
class NLUFeature(Feature):
    ...