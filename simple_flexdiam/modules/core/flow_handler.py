from __future__ import annotations
from abc import abstractmethod
from typing import Type, TYPE_CHECKING

from aaambos.core.module.feature import FeatureNecessity, Feature
from aaambos.std.extensions.message_cache.message_interest import MessageInterest
from attrs import define

from simple_flexdiam.modules.core.features import ObserverReq
from simple_flexdiam.modules.core.observer import ObserverSpace
if TYPE_CHECKING:
    from simple_flexdiam.modules.core.timeboard import TimeBoard
from simple_flexdiam.modules.simple_flexdiam_module import SimpleFlexdiamConfig, FlowHandlerConfig


# Abstract Version of the DMProper in Flexdiam


class FlowHandler:
    observer: ObserverSpace
    timeboard: TimeBoard

    def __init__(self, log, ext, flexdiam_config: SimpleFlexdiamConfig):
        self.log = log
        self.ext = ext
        self.config = flexdiam_config.flow_handler
        self.flexdiam_config = flexdiam_config
        ...

    @classmethod
    @abstractmethod
    def provides_features(cls, config: SimpleFlexdiamConfig) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        ...

    @classmethod
    @abstractmethod
    def requires_features(cls, config: SimpleFlexdiamConfig) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        ...

    @classmethod
    @abstractmethod
    def observer_requirements(cls, config: SimpleFlexdiamConfig) -> list[ObserverReq]:
        ...

    async def initialize(self, observer: ObserverSpace, timeboard: TimeBoard):
        self.observer = observer
        self.timeboard = timeboard
        observer.set_flow_handler(self)

    @abstractmethod
    async def process_entry_point(self, entry_point, data, timeboard):
        ...

    @classmethod
    @abstractmethod
    def get_config_class(cls, config: SimpleFlexdiamConfig) -> Type[FlowHandlerConfig]:
        ...

    async def step(self):
        ...

    async def add_xmits(self, xmits):
        ...