from __future__  import annotations
from abc import abstractmethod
from typing import Any, Tuple, Type, TYPE_CHECKING

from aaambos.core.module.feature import Feature, FeatureNecessity

from simple_flexdiam.modules.core.features import TierReq
from simple_flexdiam.modules.core.tier import ObservationType, Event, Tier
from simple_flexdiam.modules.core.timeboard import TimeBoard
from simple_flexdiam.modules.simple_flexdiam_module import SimpleFlexdiamConfig

if TYPE_CHECKING:
    from simple_flexdiam.modules.std.xmits.nlg_xmit import NLGXMit
    from simple_flexdiam.modules.std.observers.natural_language_generation_observer import NLGObserverConfigType


class NLGScore:

    @abstractmethod
    def get_score(self, other_scores: list["NLGScore"], score_range: Tuple[float, float], observer_config: NLGObserverConfigType) -> float:
        ...


class NLGRequest:

    @classmethod
    @abstractmethod
    def default_nlg_behavior_class(cls) -> Type[NLGProducer] | Type[NLGDelayedProducer]:
        ...


class NLGProducer:

    def __init__(self, log, nlg_observer_config: NLGObserverConfigType, plus_config: dict[str, Any], *args, **kwargs):
        self.log = log
        self.nlg_observer_config: NLGObserverConfigType = nlg_observer_config
        self.plus_config: dict[str, Any] = plus_config

    @abstractmethod
    def can_produce(self, request: NLGRequest) -> bool:
        ...

    @abstractmethod
    async def produce(self, request: NLGRequest, score_only: bool = False, **kwargs) -> Tuple[Any, NLGScore]:
        ...


class NLGDelayedProducer:

    def __init__(self, log, nlg_observer_config: NLGObserverConfigType, plus_config: dict[str, Any], *args, **kwargs):
        self.log = log
        self.nlg_observer_config: NLGObserverConfigType = nlg_observer_config
        self.plus_config: dict[str, Any] = plus_config

    @abstractmethod
    def can_produce(self, request: NLGRequest) -> bool:
        ...

    @abstractmethod
    async def prepare(self, request: NLGRequest, score_only: bool = False, **kwargs) -> Tuple[Tuple[str, Event] | list[Tuple[str, Event]], NLGScore]:
        ...

        """
        You can add new events to a tier:
        ```
        my_event = IntervalEvent(start_point=datetime.now(), data={}, short_view="shortInfoHereForGUI")  # end_point=datetime.now() + timedelta(seconds=10)
        my_point_event = PointEvent(time_point=datetime.now(), data={}, short_view="shortInfoHereForGUI")
        ```
        """

    @abstractmethod
    async def handle_event(self, tier: Tier, event: Event, nlg_xmit: NLGXMit, timeboard: TimeBoard, **kwargs) -> Tuple[bool, Any, NLGScore]:
        ...
        """You can add and update events via the timeboard argument
        ```
        await timeboard.add_event(ExampleTier.tier_name, my_event)
        await timeboard.add_event(OtherExampleTier.tier_name, my_point_event)
        ...
        my_event.end_point = datetime.now() + timedelta(seconds=10)
        await timeboard.update_event(ExampleTier.tier_name, my_event)
        ```
        """

    @classmethod
    @abstractmethod
    def get_tiers(cls, config: SimpleFlexdiamConfig, observer_config: dict) -> dict[TierReq, ObservationType | list[
        ObservationType]] | dict[str, ObservationType | list[ObservationType]]:
        return {}


registered_nlg_request_types: dict[str, Type[NLGRequest]] = {}


def register_nlg_behavior(behavior_class: Type[NLGRequest]):
    registered_nlg_request_types[behavior_class.__name__] = behavior_class
    return behavior_class

