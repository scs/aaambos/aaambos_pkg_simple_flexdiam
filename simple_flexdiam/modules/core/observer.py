from __future__ import annotations

import traceback
import uuid
from abc import abstractmethod
from typing import TYPE_CHECKING, Type

from simple_flexdiam.modules.issue_flow_handler.xmit import XMit

if TYPE_CHECKING:
    from aaambos.core.module.feature import Feature, FeatureNecessity
    from simple_flexdiam.modules.core.flow_handler import FlowHandler
    from simple_flexdiam.modules.core.timeboard import TimeBoard
    from simple_flexdiam.modules.core.tier import Tier, ObservationType
    from simple_flexdiam.modules.core.features import TierReq
    from simple_flexdiam.modules.simple_flexdiam_module import SimpleFlexdiamConfig


class Observer:
    flow_handler: FlowHandler

    def __init__(self, log, ext, process_entry_point, timeboard: TimeBoard, observer_config: dict):
        self.log = log
        self.ext = ext
        self.process_entry_point = process_entry_point
        self.timeboard = timeboard
        self.observer_config = observer_config
        self.uuid = uuid.uuid4().hex

    @classmethod
    @abstractmethod
    def provides_features(cls, config: SimpleFlexdiamConfig, observer_config: dict = ...) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        ...

    @classmethod
    @abstractmethod
    def requires_features(cls, config: SimpleFlexdiamConfig, observer_config: dict = ...) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        ...

    @classmethod
    @abstractmethod
    def get_tiers(cls, config: SimpleFlexdiamConfig, observer_config: dict) -> dict[TierReq, ObservationType | list[ObservationType]] | dict[str,  ObservationType | list[ObservationType]]:
        """Indicate the interests in tiers. Either the tiers to generate with the `TierReq` class or regex (string) to match multiple already created tiers.

        :param config: config of the module
        :param observer_config: specific config from the flow handler and/or config.
        :return:
        """
        ...

    async def initialize(self):
        ...

    async def step(self):
        ...

    def set_flow_handler(self, flow_handler: FlowHandler):
        self.flow_handler = flow_handler

    async def handle_new_event(self, tier: Tier, event):
        """is called by the tiers."""
        ...

    async def handle_event_update(self, tier: Tier, event):
        """is called by the tiers."""
        ...

    async def add_xmit(self, xmit):
        """is called by the flow handler"""
        ...

    @classmethod
    def potential_xmit_classes(cls) -> list[Type[XMit]]:
        return []


class ObserverSpace:
    """Class that stores all observers."""

    def __init__(self, log, ext, config: SimpleFlexdiamConfig, timeboard: TimeBoard, process_entry_point):
        self.log = log
        self.ext = ext
        self.config = config
        self.timeboard = timeboard
        self.observers = []
        self.process_entry_point = process_entry_point
        for obs_req in self.config.settings.observers:
            observer = obs_req.observer_class(self.log, self.ext, process_entry_point, self.timeboard, obs_req.observer_config)
            self.observers.append(observer)
        self.obs_dict = {obs.__class__.__name__: obs for obs in self.observers}

    async def initialize(self):
        for observer in self.observers:
            await observer.initialize()
        ...

    async def step(self):
        for observer in self.observers:
            try:
                # TODO record/count errors?
                await observer.step()
            except Exception:
                self.log.error(traceback.format_exc())

    def set_flow_handler(self, flow_handler: FlowHandler):
        for obs in self.observers:
            obs.set_flow_handler(flow_handler)

    def get_observer(self, observer: Type[Observer]) -> Observer | None:
        if observer.__name__ in self.obs_dict:
            return self.obs_dict[observer.__name__]
        return None
