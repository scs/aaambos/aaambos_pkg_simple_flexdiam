from __future__ import annotations

import uuid
from abc import abstractmethod
from collections import deque
from datetime import datetime
from enum import Enum
from typing import Any, TYPE_CHECKING, Generator

from aaambos.core.communication.promise import CommunicationPromise
from aaambos.core.communication.service import CommunicationService
from aaambos.std.communications.attributes import MsgPayloadWrapperCallbackReceivingAttribute, MsgTopicSendingAttribute
from attrs import define, field

from simple_flexdiam.modules.core.features import TierReq
if TYPE_CHECKING:
    from simple_flexdiam.modules.core.observer import Observer


@define(kw_only=True)
class Event:
    data: Any
    short_view: str = ""
    id: str = field(factory=lambda: uuid.uuid4().hex)


@define(kw_only=True)
class PointEvent(Event):
    time_point: datetime
    ...


@define(kw_only=True)
class IntervalEvent(Event):
    start_point: datetime
    end_point: None | datetime = None
    ...


class ObservationType(Enum):
    OBSERVE = "observe"
    """handle_new_event is called when a new input is received."""
    READ = "read"
    """observer wants to read already received data from the tier."""
    WRITE = "write"
    """observer wants to add new events to the tier."""


class Tier:
    """Stores time events. Receive and send messages via the communication service that are put on the tier via Point events."""
    com: MsgTopicSendingAttribute | MsgPayloadWrapperCallbackReceivingAttribute | CommunicationService

    def __init__(self, com, tier_req: TierReq, tier_size: int, prm):
        self.com = com
        self.req = tier_req
        self.name = tier_req.tier_name
        self.prm = prm
        self.registered_observers: dict[ObservationType, list[Observer]] = {
            ObservationType.WRITE: [],
            ObservationType.READ: [],
            ObservationType.OBSERVE: [],
        }
        self.promises_to_send: list[CommunicationPromise] = []
        self.is_util_tier = self.req.is_util
        self.events = deque(maxlen=tier_size)

    async def add_event(self, event):
        self.events.append(event)
        for promise in self.promises_to_send:
            if isinstance(event.data, promise.communication_unit.payload_wrapper):
                await self.com.send(promise.settings.topic, event.data)
        for obs in self.registered_observers[ObservationType.OBSERVE]:
            await obs.handle_new_event(self, event)

    async def update_event(self, event):
        for promise in self.promises_to_send:
            if isinstance(event.data, promise.communication_unit.payload_wrapper):
                await self.com.send(promise.settings.topic, event.data)
        for obs in self.registered_observers[ObservationType.OBSERVE]:
            await obs.handle_event_update(self, event)

    def get_events_in_interval(self, start: datetime, end: datetime) -> Generator[Event]:
        for event in reversed(self.events):
            if isinstance(event, IntervalEvent):
                if event.end_point is None:
                    if end < event.start_point:
                        return
                    else:
                        yield event
                elif start > event.end_point or event.start_point > end:
                    pass
                else:
                    yield event
            else:
                if start < event.time_point < end:
                    yield event
                else:
                    return

    def get_last_event(self) -> Event | None:
        if self.events:
            return self.events[-1]
        return None

    async def com_callback(self, topic, msg):
        if hasattr(msg, "time"):
            time = msg.time
        else:
            time = datetime.now()
        event = PointEvent(data=msg, time_point=time)
        self.events.append(event)
        for obs in self.registered_observers[ObservationType.OBSERVE]:
            await obs.handle_new_event(self, event)

    async def initialize(self):
        for com_in_feature in self.req.com_in_features:
            for promise in com_in_feature.required_input:
                # TODO check if promise is from IU then other callback with more parameters
                if self.prm[promise.suggested_leaf_topic].settings:
                    await self.com.register_callback_for_promise(self.prm[promise.suggested_leaf_topic], self.com_callback)

        for com_out_features in self.req.com_out_features:
            for promise in com_out_features.provided_output:
                if self.prm[promise.suggested_leaf_topic].settings:
                    self.promises_to_send.append(self.prm[promise.suggested_leaf_topic])
                else:
                    ...  # promise (out) is never required from other module, so do not send it.

    async def register_observer(self, observer: Observer, observation_type: ObservationType | list[ObservationType]):
        if isinstance(observation_type, list):
            for obs_type in observation_type:
                self.registered_observers[obs_type].append(observer)
        else:
            self.registered_observers[observation_type].append(observer)
