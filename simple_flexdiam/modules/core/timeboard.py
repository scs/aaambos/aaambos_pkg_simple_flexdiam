from __future__ import annotations

from typing import TYPE_CHECKING

from aaambos.core.module.feature import FeatureNecessity, Feature

from simple_flexdiam.modules.core.tier import Tier, ObservationType

if TYPE_CHECKING:
    from simple_flexdiam.modules.simple_flexdiam_module import SimpleFlexdiamConfig


class TimeBoard:
    """Class that manages all tiers."""

    def __init__(self, log, config: SimpleFlexdiamConfig, com, prm):
        self.log = log
        self.config = config
        self.com = com
        self.prm = prm
        self.tiers: dict[str,  Tier] = {}

    @classmethod
    def provides_features(cls, config: SimpleFlexdiamConfig) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        ...

    @classmethod
    def requires_features(cls, config: SimpleFlexdiamConfig) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        ...

    async def initialize(self):
        for tier_req in self.config.settings.tiers:
            tier = Tier(self.com, tier_req, self.config.tier_size, prm=self.prm)
            await tier.initialize()
            self.tiers[tier.name] = tier

    async def step(self):
        ...

    async def register_observer(self, tier_name, observer, observer_type: ObservationType | list [ObservationType]):
        if tier_name in self.tiers:
            await self.tiers[tier_name].register_observer(observer, observer_type)

    # TODO unregister observer

    async def add_event(self, tier_name: str, event):
        if tier_name in self.tiers:
            await self.tiers[tier_name].add_event(event)
        else:
            ...
            self.log.warning(f"added event to tier that does not exist {tier_name=}, {self.tiers.keys()}")
            # log

    async def update_event(self, tier_name: str, event):
        if tier_name in self.tiers:
            await self.tiers[tier_name].update_event(event)
        else:
            ...
            # log
