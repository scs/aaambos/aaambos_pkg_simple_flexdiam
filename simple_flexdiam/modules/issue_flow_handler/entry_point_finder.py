from __future__ import annotations

from abc import abstractmethod
from typing import TYPE_CHECKING

from simple_flexdiam.modules.issue_flow_handler.xmit import XMit

if TYPE_CHECKING:
    from simple_flexdiam.modules.issue_flow_handler.issue_fh_config import IssueFHConfig
    from simple_flexdiam.modules.simple_flexdiam_module import SimpleFlexdiamConfig


class EntryPointFinder:

    def __init__(self, log, config: IssueFHConfig, flexdiam_config: SimpleFlexdiamConfig, set_root_func):
        self.log = log
        self.config = config
        self.flexdiam_config = flexdiam_config
        self.issues_changed = True
        self.set_root_func = set_root_func

    @abstractmethod
    def setup_root_issue(self, root):
        ...

    @abstractmethod
    def find_issue_with_successful_entry_point(self, entry_point, ep_data, timeboard, context) -> list[XMit]:
        ...

    @abstractmethod
    def do_prompt_request(self, context, timeboard) -> list[XMit]:
        ...

    def update_issue_view(self) -> bool:
        if self.issues_changed:
            self.issues_changed = False
            return True
        return False
