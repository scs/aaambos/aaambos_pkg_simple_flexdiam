from __future__ import annotations

import uuid
from abc import abstractmethod
from copy import deepcopy
from enum import Enum
from typing import Literal, Generator, Type, TYPE_CHECKING, Any

from aaambos.core.module.feature import FeatureNecessity, Feature

from simple_flexdiam.modules.core.features import ObserverReq
from simple_flexdiam.modules.issue_flow_handler.issue_fh_config import IssueFHConfig
from simple_flexdiam.modules.issue_flow_handler.xmit import XMit
from simple_flexdiam.modules.simple_flexdiam_module import SimpleFlexdiamConfig

if TYPE_CHECKING:
    from simple_flexdiam.modules.issue_flow_handler.issue_flow_handler import Context


class IssueState(Enum):
    SHADOW = "shadow"
    NEW = "new"
    INVOKED = "invoked"  # not used
    ENTERED = "entered"
    WAIT_FOR_CHILDREN = "wait_for_children"  # not used
    FULFILLED = "fulfilled"
    FAILED = "failed"
    OBSOLETE = "obsolete"  # not used, maybe New and invoked issues which never entered

    def is_shadow(self) -> bool:
        return self == self.SHADOW

    def is_in_setup(self) -> bool:
        return self in [self.NEW, self.INVOKED]

    def is_running(self) -> bool:
        return self in [self.ENTERED, self.WAIT_FOR_CHILDREN]

    def is_terminated(self) -> bool:
        return self in [self.FULFILLED, self.FAILED, self.OBSOLETE]

    def is_ended(self) -> bool:
        """by hand ended"""
        return self in [self.FULFILLED, self.FAILED]


class Locals:

    def __repr__(self):
        return ", ".join(f"{a}={getattr(self, a)!r}" for a in dir(self) if not a.startswith("__"))


class IssueOutput:

    def __init__(self, issue: Issue):
        self.issue = issue
        self._assigned_children: list[tuple[Type[Issue], str | Ellipsis, dict | Ellipsis]] = []
        self._xmits = []
        self._usages = []
        self._sub_outputs = {}

    def add_child(self, child_class: Type[Issue], abstract_name: str = ..., config: dict = ...):
        """call this to add a child to the issue"""
        self._assigned_children.append((child_class, abstract_name, config))

    def add_xmit(self, xmit: XMit):
        """call this inside an issue to add an output"""
        self._xmits.append(xmit)

    def mention_usage(self, usage_ref, importance: int = 1):
        self._usages.append((usage_ref, importance))

    def has_assigned_children(self) -> bool:
        return len(self._assigned_children) > 0

    def get_assigned_children(self) -> list[tuple[Type[Issue], str | Ellipsis, dict | Ellipsis]]:
        return self._assigned_children

    def has_xmits(self) -> bool:
        return len(self._xmits) > 0

    def get_xmits(self) -> list[XMit]:
        return self._xmits

    def add_sub_issue_output(self, output, other_id):
        self._sub_outputs[other_id] = output


class Issue:
    """Represents dialog structure in a tree.

    Can be called via the entry_points or prompt_requests (timed).
    The class can not have further attributes. Write them instead in the locals, e.g.,
    ```python
    self.locals.my_var = 1
    ```
    instead of
    ```python
    self.my_var = 1
    ```

    Set an end state vie the set_end_state method:
    ```python
    self.set_end_state(IssueState.FAILED)
    # or
    self.set_end_state(IssueState.FULFILLED)
    ```

    You can add child processes via the output in the handle_entry_point and handle_prompt_request:
    ```python
    output.add_child(IssueGreet, abstract_name="my_greet_issue", config={})
    ```
    Or a speech genreation via xmits:
    ```
    speech_gen = SpeechGenerationControl(
        control=Controls.START,
        id=uuid.uuid4(),
        text=ep_data.utterance,
    )
    xmit = SpeechGenerationXMit(speech_generation=speech_gen, floor_yield=timedelta(seconds=1))
    output.add_xmit(xmit)
    ```
    For more details have a look at the `IssueOutput` class.
    """
    # store xmits and queued_xmits?
    # store context?
    __slots__ = "_state", "uid", "parent", "children", "abstract_name", "locals", "queued_xmits", "config"
    _state: IssueState
    """The state of the issue, either shadow, in a setup state, active, or failed or fulfiled. Set the end state via the set_end_state method."""
    uid: str
    """unique identifier of an issue. Not the same for an instance of a shadow issue."""
    parent: Issue | None
    """only the root issue has no parent."""
    children: list[Issue]
    """list of all children, including shadow issues. There are utility methods to get subsets of it."""
    abstract_name: str
    """the abstract name of the issue, does not need to be unique."""
    locals: Locals
    """the local variable store. Use it instead of variables/attributes."""
    queued_xmits: list[XMit]
    """Currently not implemented, but later it can contain the xmits that are queued/not commited."""
    config: dict
    """The configuration of the issue done by the parent issue."""

    def __init__(self, abstract_name: str, parent: Issue | None,  config: dict, as_shadow: bool = False, _children = ..., _locals = ..., _queued_xmits = ..., _uid = ..., _state = ...):
        self._state = (IssueState.SHADOW if as_shadow else IssueState.NEW) if _state is ... else _state
        self.uid = uuid.uuid4().hex if _uid is ... else _uid
        self.parent = parent
        self.children = [] if _children is ... else _children
        self.abstract_name = abstract_name
        self.config = config
        # if not as_shadow:
        self.locals = Locals() if _locals is ... else _locals
        self.queued_xmits = [] if _queued_xmits is ... else _queued_xmits

    @classmethod
    @abstractmethod
    def get_init_shadow_children(cls, config: SimpleFlexdiamConfig) -> list[Type[Issue] | tuple[Type[Issue], dict]]:
        """Provide the shadow isssue classes that are initialised with the issue. It can also contain a config [(MyIssue, {}), ...] instead of [MyIssue, ...]"""
        ...

    @classmethod
    @abstractmethod
    def potential_children_classes(cls, config: SimpleFlexdiamConfig) -> list[Type[Issue]]:
        """List all classes of possible child issues that the issue might want to add. Required for setup of flexdiam."""
        ...

    @classmethod
    @abstractmethod
    def entry_points_of_interest(cls) -> list[str]:
        """The entry points (their names) which the issue wants to be called."""
        ...

    @classmethod
    @abstractmethod
    def potential_xmit_classes(cls) -> list[Type[XMit]]:
        """The potential added xmit classes. Required for setup."""
        ...

    @abstractmethod
    def initialize(self):
        # TODO add context variable
        ...


    @abstractmethod
    def handle_child_closed(self, child: Issue, context: Context, output: IssueOutput):
        """Is called when a child is closed / ended / failed/fulfilled."""
        ...

    @abstractmethod
    def can_handle_entry_point(self, entry_point: str, ep_data: Any, context: Context) -> bool:
        """Check if it can handle entry point data, e.g., check for correct intention in an NLU results."""
        ...

    @abstractmethod
    def handle_entry_point(self, entry_point: str, ep_data: Any, context: Context, output: IssueOutput):
        """Is called after can_handle_entry_point returned True."""
        ...

    @abstractmethod
    def handle_prompt_request(self, context: Context, output: IssueOutput):
        """Is called on active issues iteratively (not necessary) all."""
        ...

    @classmethod
    def further_observer_requirements(cls, issue_fh_config: IssueFHConfig) -> list[ObserverReq]:
        """Overwrite this function if the issue needs additional observers"""
        return []

    @classmethod
    def provides_features(cls, config: SimpleFlexdiamConfig) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        """Overwrite if necessary"""
        return {}

    @classmethod
    def requires_features(cls, config: SimpleFlexdiamConfig) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        """Overwrite if necessary"""
        return {}

    @property
    def state(self):
        return self._state

    def set_end_state(self, state: Literal[IssueState.FAILED, IssueState.FULFILLED]):
        """Call this function to set the issue as failed or fulfilled."""
        if not self._state.is_running():
            pass
            # raise ValueError(f"Issue {self.abstract_name!r} in state {self._state.name} can not be set to {state.name}:{self.abstract_name}")
        if state in [IssueState.FAILED, IssueState.FULFILLED]:
            self._state = state
        else:
            raise ValueError(f"Issue end state cannot set to {state.value!r} in the issue {self.abstract_name!r}.")

    def is_active(self) -> bool:
        return self._state.is_in_setup() or self._state.is_running()

    def invalidate(self):
        """Utility method is called after it entered an end state."""
        self.invalidate_children()
        for xmit in self.queued_xmits:
            xmit.retract()

    def invalidate_children(self):
        """Utility method is called after it entered an end state."""
        for c in self.active_children():
            c.invalidate()
        for c in self.shadow_children():
            c.invalidate()

    def active_children(self) -> Generator[Issue]:
        for c in self.children:
            if c.state.is_running() or c.state.is_in_setup():
                yield c

    def shadow_children(self) -> Generator[Issue]:
        for c in self.children:
            if c.state.is_shadow():
                yield c

    def __repr__(self):
        return f"{self.__class__.__name__}(state={self._state.name}, ab_name={self.abstract_name!r}, locals={{{(self.locals if self.state != IssueState.SHADOW else 'None')!r}}}, n_children={len(self.children)})"

    def __deepcopy__(self, memo):
        return self.__class__(
            abstract_name=deepcopy(self.abstract_name, memo),
            parent=self.parent,
            config=deepcopy(self.config, memo),
            as_shadow=self._state.is_shadow(),
            _children=[c for c in self.children],
            _locals=deepcopy(self.locals, memo),
            _state=deepcopy(self._state, memo),
            _queued_xmits=[xmit for xmit in self.queued_xmits],
            _uid=deepcopy(self.uid, memo),
        )
