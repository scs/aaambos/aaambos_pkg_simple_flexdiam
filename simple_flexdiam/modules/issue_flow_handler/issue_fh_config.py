from __future__ import annotations

from typing import Type, Callable, TYPE_CHECKING

from attrs import define, field

from simple_flexdiam.modules.simple_flexdiam_module import FlowHandlerConfig
if TYPE_CHECKING:
    from simple_flexdiam.modules.issue_flow_handler.entry_point_finder import EntryPointFinder
    from simple_flexdiam.modules.issue_flow_handler.issue import Issue


@define(kw_only=True)
class IssueFHConfig(FlowHandlerConfig):
    root_issue: Type[Issue]
    """the root issue."""
    shadow_root_issues: list[Type[Issue] | list[Type[Issue], dict] | Callable[[], list[Type[Issue] | list[Type[Issue] | dict]]]]
    """list of issue classes or functions that provide them that are set as shadow issues of the root issue."""
    entry_point_finder: Type[EntryPointFinder]
    """class used for finding the next best entry point."""
    prompt_request_interval_seconds: float = 0.5
    """how often to reiterate the prompt request"""
    ignore_hypotheses: bool = True
    """process only final nlu results"""
    issue_output_comparison: Callable | None = None
    plus: dict = field(factory=lambda:dict())
    """Further config, for example for issues."""

