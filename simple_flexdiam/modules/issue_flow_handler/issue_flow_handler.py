import asyncio
from collections import defaultdict
from datetime import datetime, timedelta
from typing import Type, Generator

from aaambos.core.module.feature import Feature, FeatureNecessity

from simple_flexdiam.modules.core.definitions import AsrResultState
from simple_flexdiam.modules.core.features import ObserverReq, add_req_to_reqs
from simple_flexdiam.modules.core.flow_handler import FlowHandler
from simple_flexdiam.modules.core.observer import ObserverSpace
from simple_flexdiam.modules.core.timeboard import TimeBoard
from simple_flexdiam.modules.issue_flow_handler.entry_point_finder import EntryPointFinder
from simple_flexdiam.modules.issue_flow_handler.issue import Issue
from simple_flexdiam.modules.issue_flow_handler.issue_fh_config import IssueFHConfig
from simple_flexdiam.modules.simple_flexdiam_module import SimpleFlexdiamConfig, FlowHandlerConfig
from simple_flexdiam.modules.std.observers.issue_gui import issue_gui_obs_req
from simple_flexdiam.modules.std.observers.agent_words_observer import agent_words_observer_req
from simple_flexdiam.modules.std.observers.floor_observer import floor_observer_req
from simple_flexdiam.modules.std.observers.nlu_entry_point_observer import nlu_entry_point_observer_req, \
    NLU_PARSE_ENTRY_POINT
from simple_flexdiam.modules.std.observers.speech_generation_observer import speech_generation_observer_req, \
    SpeechGenerationObserver
from simple_flexdiam.modules.std.observers.user_words_observer import user_words_observer_req


class Context:
    ...


class IssueFlowHandler(FlowHandler):
    root_issue: Issue
    config: IssueFHConfig
    ep_finder = EntryPointFinder
    speech_gen_obs: SpeechGenerationObserver
    context: Context
    prompt_offset: timedelta
    last_prompt: datetime

    @classmethod
    def provides_features(cls, config: SimpleFlexdiamConfig) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        features = {}
        for issue in cls._potential_issues(config.flow_handler.root_issue, config):
            add_features = issue.provides_features(config)
            if add_features:
                features.update(features)
        return features

    @classmethod
    def requires_features(cls, config: SimpleFlexdiamConfig) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        features = {}
        for issue in cls._potential_issues(config.flow_handler.root_issue, config):
            add_features = issue.requires_features(config)
            if add_features:
                features.update(features)
        return features

    @classmethod
    def observer_requirements(cls, config: SimpleFlexdiamConfig) -> list[ObserverReq]:
        base_observers: list[ObserverReq] = [
            agent_words_observer_req,
            floor_observer_req,
            nlu_entry_point_observer_req,
            user_words_observer_req,
        ]
        if config.gui:
            base_observers.append(issue_gui_obs_req)
        # config.flow_handler: IssueFHConfig
        for issue in cls._potential_issues(config.flow_handler.root_issue, config):
            further_obs = issue.further_observer_requirements(config.flow_handler)
            for obs in further_obs:
                add_req_to_reqs(obs, base_observers)
            xmit_classes = issue.potential_xmit_classes()
            for xmit_class in xmit_classes:
                add_req_to_reqs(ObserverReq(observer_class=xmit_class.get_observer_class(), observer_config={}), base_observers)
        visited = set()
        to_visit = base_observers.copy()
        while to_visit:
            obs = to_visit.pop(0)
            if obs.observer_class.__name__ not in visited:
                visited.add(obs.observer_class.__name__)
                xmit_classes = obs.observer_class.potential_xmit_classes()
                for xmit_class in xmit_classes:
                    req = add_req_to_reqs(ObserverReq(observer_class=xmit_class.get_observer_class(), observer_config={}), base_observers)
                    if req:
                        to_visit.append(req)
        return base_observers

    async def process_entry_point(self, entry_point, data, timeboard):
        if entry_point == NLU_PARSE_ENTRY_POINT and data.state == AsrResultState.HYPOTHESIS and self.config.ignore_hypotheses:
            return
        xmits = self.ep_finder.find_issue_with_successful_entry_point(entry_point, data, self.timeboard, self.context)
        await self.add_xmits(xmits)

    async def initialize(self, observer: ObserverSpace, timeboard: TimeBoard):
        await super().initialize(observer, timeboard)
        self.last_prompt = datetime.now()
        self.prompt_offset = timedelta(seconds=int(self.config.prompt_request_interval_seconds), milliseconds=int(self.config.prompt_request_interval_seconds*1000 % 1000))
        self.context = Context()
        self.speech_gen_obs = self.observer.get_observer(SpeechGenerationObserver)
        self.ep_finder = self.config.entry_point_finder(self.log, self.config, self.flexdiam_config, self.set_root)
        self.root_issue = self.config.root_issue(abstract_name="root", parent=None, config={}, as_shadow=False)
        self.ep_finder.setup_root_issue(self.root_issue)

    async def step(self):
        if self.speech_gen_obs.can_generate():
            if self.last_prompt + self.prompt_offset < datetime.now():
                self.last_prompt = datetime.now()
                xmits = self.ep_finder.do_prompt_request(self.context, self.timeboard)
                await self.add_xmits(xmits)
                await self.speech_gen_obs.mention_prompt_request()
        else:
            # to prioritize entry points instead of prompt request after floor yield and speech_generation
            self.last_prompt = datetime.now()

    async def add_xmits(self, xmits):
        for xmit in xmits:
            observer = self.observer.get_observer(xmit.get_observer_class())
            if observer:
                await observer.add_xmit(xmit)

    @classmethod
    def _potential_issues(cls, start_issue_class: Type[Issue], flexdiam_config: SimpleFlexdiamConfig) -> Generator[Type[Issue], None, None]:
        visited = {start_issue_class.__name__}
        stack = [start_issue_class]
        yield start_issue_class
        while stack:
            issue_class = stack.pop()
            potential_children_classes = issue_class.potential_children_classes(flexdiam_config)
            if potential_children_classes:
                for child_class in potential_children_classes:
                    if child_class.__name__ not in visited:
                        visited.add(child_class.__name__)
                        stack.append(child_class)
                        yield child_class

    def set_root(self, new_root: Issue):
        self.root_issue = new_root

    @classmethod
    def get_config_class(cls, config: SimpleFlexdiamConfig) -> Type[FlowHandlerConfig]:
        return IssueFHConfig
