from __future__ import annotations

from typing import Tuple, TYPE_CHECKING

from simple_flexdiam.modules.core.timeboard import TimeBoard
from simple_flexdiam.modules.issue_flow_handler.issue import Issue, IssueOutput
from simple_flexdiam.modules.issue_flow_handler.issue_flow_handler import Context

if TYPE_CHECKING:
    from simple_flexdiam.modules.issue_flow_handler.selective_entry_point_finder import SelectiveEntryPointFinder


def select_first_issue(cloned_issues: list[Tuple[bool, int, Issue, IssueOutput, Context, bool]], entry_point, ep_data, timeboard: TimeBoard, context_before: Context, ep_finder: SelectiveEntryPointFinder, xmit_child_parent, **kwargs) -> list[int]:
    return [0]


def select_issue_with_most_xmits(cloned_issues: list[Tuple[bool, int, Issue, IssueOutput, Context, bool]], entry_point, ep_data, timeboard: TimeBoard, context_before: Context, ep_finder: SelectiveEntryPointFinder, xmit_child_parent, **kwargs) -> list[int]:
    # choose issue with most xmits; if equal between two prioritize not shadows
    if len(cloned_issues) == 1:
        return [0]
    current_max = (-1, -1, False)
    selected_idxs = []
    for idx, (is_shadow, ref, issue, output, context, state_changed) in enumerate(cloned_issues):
        n_xmits = len(xmit_child_parent[ref][0]) if ref in xmit_child_parent else -2
        # if state_changed and issue.state.is_terminated() and n_xmits == 0:
        #     ep_finder.log.trace(f"issue {issue.abstract_name} is terminated add it to output")
        #     selected_idxs.append(idx)
        #     continue
        ep_finder.log.trace(f"issue {issue.abstract_name} with {n_xmits} xmits ({state_changed=}, state={issue.state.name})") if not entry_point.startswith("_") else None
        if current_max[0] < n_xmits:
            current_max = (n_xmits, idx, current_max[2] or not is_shadow)
        elif current_max[0] == n_xmits and not current_max[2] and not is_shadow:
            current_max = (n_xmits, idx, is_shadow)

    if current_max[0] == -1:
        return selected_idxs
    selected_idxs.append(current_max[1])
    return selected_idxs


# select with most impact / score

# include issues with flag "always add"

# how to handle shadows

# min/max with use nlu parts (?)

