from __future__ import annotations

import traceback
import uuid
from copy import deepcopy
from functools import partial
from typing import Tuple, Callable

from simple_flexdiam.modules.core.timeboard import TimeBoard
from simple_flexdiam.modules.issue_flow_handler.select_clone_functions import select_issue_with_most_xmits
from simple_flexdiam.modules.issue_flow_handler.entry_point_finder import EntryPointFinder
from simple_flexdiam.modules.issue_flow_handler.issue import Issue, IssueState, IssueOutput
from simple_flexdiam.modules.issue_flow_handler.issue_fh_config import IssueFHConfig
from simple_flexdiam.modules.issue_flow_handler.issue_flow_handler import Context
from simple_flexdiam.modules.issue_flow_handler.xmit import XMit
from simple_flexdiam.modules.simple_flexdiam_module import SimpleFlexdiamConfig

HALLO = 1


class SelectiveEntryPointFinder(EntryPointFinder):

    def __init__(self, log, config: IssueFHConfig, flexdiam_config: SimpleFlexdiamConfig,
                 set_root_func: Callable[[Issue], None]):
        super().__init__(log, config, flexdiam_config, set_root_func)
        self.active_issues = []
        self.compare_issue_outputs = config.issue_output_comparison if config.issue_output_comparison else select_issue_with_most_xmits

    def setup_root_issue(self, root):
        root._state = IssueState.ENTERED
        self.create_shadows(root)
        root.initialize()
        self.active_issues = [root]

    @staticmethod
    def create_cloned_issue(issue: Issue) -> Issue:
        return deepcopy(issue)

    def insert_clones(self, cloned_issue, new_children, updated_parents):
        self.insert_clone(cloned_issue, updated_parents[-1][0] if updated_parents else None)
        first = False
        for parent, child in reversed(updated_parents):
            if first:
                self.insert_clone(child, parent)
                child = parent
            else:
                first = True
        if first:
            self.insert_clone(child, None)
            # TODO update parents locals from previous one (because maybe two clones of the parents are there)
            # TODO merge children or parents (how?)
        self.active_issues.extend(new_children)

    def insert_clone(self, cloned_issue, parent=None):
        idx = -1
        for i, a in enumerate(self.active_issues):
            if a.uid == cloned_issue.uid:
                # self.log.trace(f"Same uuid {a} {cloned_issue} - {a.uid}")
                idx = i
                break

        if idx == -1:
            # self.log.trace(f"Did not found original issue. Add it - {cloned_issue}")
            self.active_issues.append(cloned_issue)
        else:
            # self.log.trace(f"Found original issue {idx} {cloned_issue} - {parent=}")
            old_issue = self.active_issues[idx]
            self.active_issues[idx] = cloned_issue
            children_to_add = []
            children_ids = set([c.uid for c in cloned_issue.children])
            for child in old_issue.children:
                if child.uid not in children_ids:
                    children_to_add.append(child)
            # self.log.trace(f"Add old children that are not part of the new one: {children_to_add}")
            cloned_issue.children.extend(children_to_add)
            # self.log.trace(f"Set cloned issue as parent of following children {cloned_issue.children}")
            for child in cloned_issue.children:
                child.parent = cloned_issue

        if parent or cloned_issue.parent:
            if parent is None:
                parent = cloned_issue.parent
            # self.log.trace(f"Update parent of cloned issue: {parent}")
            found = -1
            for i, c in enumerate(parent.children):
                if c.uid == cloned_issue.uid:
                    found = i
                    break
            if found == -1:
                parent.children.append(cloned_issue)
            else:
                parent.children[found] = cloned_issue
        else:
            # self.log.trace(f"No parent - Set root {cloned_issue}")
            self.set_root_func(cloned_issue)

    def update_context(self, cloned_context, context):
        context.__dict__.update(cloned_context.__dict__)
        deleted_keys = set(context.__dict__.keys()).difference(cloned_context.__dict__.keys())
        if deleted_keys:
            for d in deleted_keys:
                del context.__dict__[d]

    def check_entry_point_on_active_issue(self, issue, cloned_issues, entry_point, ep_data, context, idx, partial_func,
                                          xmit_child_parent_store):
        if entry_point in issue.entry_points_of_interest() and issue.can_handle_entry_point(entry_point, ep_data,
                                                                                            context):
            new_xmits, new_children, updated_parents, cloned_issue = self.call_clone(cloned_issues, context, idx, issue,
                                                                                     partial_func)
            if cloned_issue:
                xmit_child_parent_store[idx] = new_xmits, new_children, updated_parents

    def realize_clones(self, cloned_issues, context, entry_point, ep_data, timeboard, xmit_child_parent_store, xmits,
                       logging=False):
        self.log.trace(f"Choose issues to realize from ({len(cloned_issues)}) {cloned_issues}") if logging else None
        selected_issues: list[int] = self.compare_issue_outputs(cloned_issues, entry_point, ep_data, timeboard,
                                                                context, self, xmit_child_parent_store)

        self.log.trace(f"Selected issues: {selected_issues}") if logging else None
        for i in selected_issues:
            self.issues_changed = True
            assert i < len(cloned_issues)
            is_shadow, idx, cloned_issue, issue_output, cloned_context, state_changed = cloned_issues[i]
            self.log.trace(f"Realize issue for {entry_point=} with {ep_data=} from {cloned_issue}") if logging else None
            if idx in xmit_child_parent_store:
                new_xmits, *parent_children = xmit_child_parent_store[idx]
                self.log.trace(f"Add children {parent_children[0]}") if logging and parent_children[0] else None
                self.insert_clones(cloned_issue, *parent_children)
                self.log.trace(f"Add xmits {new_xmits}") if logging and new_xmits else None
                xmits.extend(new_xmits)
            # update context
            # deep update?
            self.update_context(cloned_context, context)

    def find_issue_with_successful_entry_point(self, entry_point, ep_data, timeboard: TimeBoard, context: Context) -> \
    list[XMit]:
        # other structure. because of ö
        xmits = []
        children = []
        cloned_issues = []
        xmit_child_parent_store = {}
        try:
            # self.log.trace(f"Start search for entry point {entry_point!r} {ep_data} in {self.active_issues}.")
            partial_call_entry_point = partial(self.call_handle_entry_point, entry_point=entry_point, ep_data=ep_data)
            for idx, issue in enumerate(self.active_issues):
                if issue.state.is_terminated():
                    continue
                self.check_entry_point_on_active_issue(issue, cloned_issues, entry_point, ep_data, context, idx,
                                                       partial_call_entry_point, xmit_child_parent_store)

                for i, shadow_issue in enumerate(issue.shadow_children()):
                    self.check_entry_point_on_active_issue(shadow_issue, cloned_issues, entry_point, ep_data, context,
                                                           f"shadow_{idx}_{i}", partial_call_entry_point,
                                                           xmit_child_parent_store)
            if cloned_issues:
                self.realize_clones(cloned_issues, context, entry_point, ep_data, timeboard, xmit_child_parent_store,
                                    xmits, logging=True)
        except Exception:
            self.log.error(traceback.format_exc())

        self.active_issues.extend(children)
        return xmits

    def call_clone(self, cloned_issues, context: Context, idx: int | str, issue: Issue,
                   call_on_clone_func: partial | Callable, first_clone=True) -> Tuple[
        list[XMit], list[Issue], list[Tuple[Issue, Issue]], Issue | None]:
        # self.log.trace(f"Setup clone for {issue}")
        cloned_issue = self.create_cloned_issue(issue)
        if first_clone:
            cloned_context = deepcopy(context)
        else:
            cloned_context = context

        if cloned_issue.state.is_shadow():
            cloned_issue.uid = uuid.uuid4().hex
            cloned_issue._state = IssueState.ENTERED
        elif issue.state.is_in_setup():
            cloned_issue._state = IssueState.ENTERED

        issue_output = IssueOutput(cloned_issue)
        try:
            # self.log.trace(f"Call func for cloned issue {cloned_issue}")
            call_on_clone_func(cloned_issue, cloned_context, issue_output)
        except Exception as e:
            self.log.error(
                f"{call_on_clone_func.func.__name__ if isinstance(call_on_clone_func, partial) else call_on_clone_func.__name__} failed for {cloned_issue}")
            self.log.error(traceback.format_exc())
            if isinstance(e, AttributeError):
                attribute_name = e.args[0].split("'")[-2]
                self.log.error(f"Issues do not allow custom attributes. Store variables in 'self.locals', e.g., instead of 'self.{attribute_name}' do 'self.locals.{attribute_name}'. This is due to issue cloning.")
            if not issue.state.is_shadow():
                issue._state = IssueState.FAILED
            else:
                cloned_issues.append((False, idx, cloned_issue, issue_output, cloned_context, True))
                return [], [], [], None
        xmits, children, parents = self.process_issue_output(cloned_context, cloned_issue, issue_output)

        cloned_issues.append((issue.state.is_shadow(), idx, cloned_issue, issue_output, cloned_context,
                              issue.state != cloned_issue.state))
        return xmits, children, parents, cloned_issue

    @staticmethod
    def call_handle_entry_point(issue, context, output, entry_point, ep_data):
        # copy ep_data ??
        issue.handle_entry_point(entry_point, ep_data, context, output)

    @staticmethod
    def call_handle_child_closed(parent, context, output, child):
        parent.handle_child_closed(child, context, output)

    @staticmethod
    def call_handle_prompt_request(issue, context, output):
        issue.handle_prompt_request(context, output)

    def process_issue_output(self, context, issue: Issue, issue_output):
        self.issues_changed = True
        xmits = issue_output.get_xmits()
        updated_parents, children = [], []
        if issue.state.is_ended():
            parent_output = []
            issue.invalidate()
            if issue.parent is not None:
                # self.log.trace(f"Call handle child closed on {issue.parent.abstract_name} with {issue.abstract_name}")
                new_xmits, children, updated_parents, cloned_parent = self.call_clone(parent_output, context, None,
                                                                                      issue.parent, partial(
                        self.call_handle_child_closed, child=issue), first_clone=False)
                if cloned_parent:
                    updated_parents.append((cloned_parent, issue))
                    issue_output.add_sub_issue_output(parent_output[0][3], cloned_parent.uid)
                xmits.extend(new_xmits)
        elif issue_output.has_assigned_children():
            new_xmits, children, updated_parents = self.setup_children(issue, issue_output, context)
            xmits.extend(new_xmits)
        return xmits, children, updated_parents

    def setup_children(self, parent, issue_output: IssueOutput, context) -> tuple[
        list[XMit], list[Issue], list[Tuple[Issue, Issue]]]:
        children = []
        xmits = []
        parents = []
        for child_class, abstract_name, config in issue_output.get_assigned_children():
            # self.log.info(f"Create issue {abstract_name!r} of type {child_class.__name__} with {config=}")
            child_issue = child_class(abstract_name=abstract_name if abstract_name is not ... else child_class.__name__,
                                      parent=parent, config=config if config is not ... else {}, as_shadow=False)
            self.create_shadows(child_issue)
            parent.children.append(child_issue)
            child_issue.initialize()
            children.append(child_issue)
            child_issue_output = IssueOutput(child_issue)
            child_issue.handle_prompt_request(context, child_issue_output)
            if child_issue._state.is_in_setup():
                child_issue._state = IssueState.ENTERED
            new_xmits, grand_children, updated_parent = self.process_issue_output(context, child_issue,
                                                                                  child_issue_output)
            issue_output.add_sub_issue_output(output=child_issue_output, other_id=child_issue.uid)
            children.extend(grand_children)
            xmits.extend(new_xmits)
            # TODO do differently because of several children could lead to wrong updated parent structure
            parents.extend(updated_parent)
        return xmits, children, parents

    def create_shadows(self, issue):
        shadows = issue.get_init_shadow_children(self.flexdiam_config)
        if shadows:
            for shadow in shadows:
                if isinstance(shadow, tuple):
                    # self.log.trace(f"Create shadow {shadow.__name__!r} with config={shadow[1]}")
                    shadow_issue = shadow[0](abstract_name=shadow.__name__, parent=issue, config=shadow[1],
                                             as_shadow=True)
                else:
                    # self.log.trace(f"Create shadow {shadow.__name__!r} with config={{}}")
                    shadow_issue = shadow(abstract_name=shadow.__name__, parent=issue, config={}, as_shadow=True)
                shadow_issue.initialize()
                issue.children.append(shadow_issue)

    def do_prompt_request(self, context, timeboard) -> list[XMit]:
        terminated_children = []
        cloned_issues = []
        n_issues = len(self.active_issues)
        xmit_child_parent_store = {}
        xmits = []
        # maybe do not clone when just root is available ?
        try:
            for idx, issue in enumerate(reversed(self.active_issues)):
                cloned_issue = None
                if not issue.state.is_terminated():
                    new_xmits, new_children, updated_parents, cloned_issue = self.call_clone(cloned_issues, context,
                                                                                             n_issues - idx - 1, issue,
                                                                                             self.call_handle_prompt_request)
                    if cloned_issue:
                        xmit_child_parent_store[idx] = new_xmits, new_children, updated_parents
                if cloned_issue is None:
                    terminated_children.append(issue)
            if cloned_issues:
                self.realize_clones(cloned_issues, context, "__prompt_request__", None, timeboard,
                                    xmit_child_parent_store, xmits)

            for child in terminated_children:
                self.active_issues.remove(child)
        except Exception:
            self.log.error(traceback.format_exc())
        return xmits
