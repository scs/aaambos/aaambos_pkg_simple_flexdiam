import traceback
import uuid
from copy import deepcopy

from simple_flexdiam.modules.core.timeboard import TimeBoard
from simple_flexdiam.modules.issue_flow_handler.entry_point_finder import EntryPointFinder
from simple_flexdiam.modules.issue_flow_handler.issue import Issue, IssueOutput, IssueState
from simple_flexdiam.modules.issue_flow_handler.issue_fh_config import IssueFHConfig
from simple_flexdiam.modules.issue_flow_handler.issue_flow_handler import Context
from simple_flexdiam.modules.issue_flow_handler.xmit import XMit
from simple_flexdiam.modules.simple_flexdiam_module import SimpleFlexdiamConfig


class SimpleEntryPointFinder(EntryPointFinder):

    def __init__(self, log, config: IssueFHConfig, flexdiam_config: SimpleFlexdiamConfig, set_root_func):
        super().__init__(log, config, flexdiam_config, set_root_func)
        self.active_issues = []
        self.last_call_prompt_request_issue = ""

    def setup_root_issue(self, root):
        root._state = IssueState.ENTERED
        self.create_shadows(root)
        root.initialize()
        self.active_issues = [root]

    def find_issue_with_successful_entry_point(self, entry_point, ep_data, timeboard: TimeBoard, context: Context) -> list[XMit]:
        inactive_issues = []
        for i_r, issue in enumerate(reversed(self.active_issues)):
            xmits, new_issues = self._check_issue_for_ep(issue, entry_point, ep_data, timeboard, context)
            # TODO complex EntryPointFinder that clones issue states and only finds the best one and only realizes the best one
            self.active_issues.extend(new_issues)
            if xmits:
               return xmits
        return []

    def _check_issue_for_ep(self, issue, entry_point, ep_data, timeboard: TimeBoard, context: Context) -> tuple[list[XMit], list[Issue]]:
        new_children = []
        try:
            self.log.trace(f"check issue {issue!r}")
            self.log.trace(f"issue children { {c.abstract_name: c.state.name for c in issue.children} }")
            if not issue.state.is_terminated():
                if entry_point in issue.entry_points_of_interest() and issue.can_handle_entry_point(entry_point,
                                                                                                    ep_data, context):
                    if issue.state.is_in_setup():
                        issue._state = IssueState.ENTERED
                        self.issues_changed = True
                        return [], []
                    if issue.state.is_shadow():
                        original_shadow = issue
                        issue = deepcopy(issue)
                        issue.uid = uuid.uuid4().hex
                        original_shadow.parent.children.append(issue)
                        issue.parent = original_shadow.parent
                        issue._state = IssueState.ENTERED
                        self.log.info(f"Created active issue from shadow: {issue.abstract_name!r}")
                        self.create_shadows(issue)
                        new_children.append(issue)
                    issue_output = IssueOutput(issue)
                    issue.handle_entry_point(entry_point, ep_data, context, issue_output)
                    xmits, grand_children = self.process_issue_output(context, issue, issue_output)
                    new_children.extend(grand_children)
                    if xmits:
                        return xmits, new_children
                        # or check if should continue here
                        # then merge xmits
                    # active children should be inside the active issues
                    #for child_issue in issue.active_children():
                    #   xmits, n_c = self._check_issue_for_ep(child_issue, entry_point, ep_data, timeboard, context)
                    #    new_children.extend(n_c)
                    #    if xmits:
                    #        return xmits, new_children
                for shadow_issues in issue.shadow_children():
                    xmits, n_c = self._check_issue_for_ep(shadow_issues, entry_point, ep_data, timeboard, context)
                    new_children.extend(n_c)
                    if xmits:
                        return xmits, new_children
        except Exception:
            self.log.error(traceback.format_exc())
            issue._state = IssueState.FAILED
            issue.invalidate()
            # TODO add handle child closed
        return [], new_children

    def process_issue_output(self, context, issue: Issue, issue_output):
        self.issues_changed = True
        new_children = []
        xmits = issue_output.get_xmits()
        if issue.state.is_ended():
            issue.invalidate()
            if issue.parent is not None:
                child_closed_output = IssueOutput(issue.parent)
                self.log.info(f"Call handle child closed on {issue.parent.abstract_name} with {issue.abstract_name}")
                issue.parent.handle_child_closed(issue, context, child_closed_output)
                new_xmits, new_children = self.process_issue_output(context, issue.parent, child_closed_output)
                xmits.extend(new_xmits)
        elif issue_output.has_assigned_children():
            new_xmits, new_children = self.setup_children(issue, issue_output, context)
            xmits.extend(new_xmits)
        return xmits, new_children

    def setup_children(self, issue, issue_output, context) -> tuple[list[XMit], list[Issue]]:
        new_children = []
        xmits = []
        for child_class, abstract_name, config in issue_output.get_assigned_children():
            self.log.info(f"Create issue {abstract_name!r} of type {child_class.__name__} with {config=}")
            child_issue = child_class(abstract_name=abstract_name if abstract_name is not ... else child_class.__name__,
                                      parent=issue, config=config if config is not ... else {}, as_shadow=False)
            self.create_shadows(child_issue)
            issue.children.append(child_issue)
            child_issue.initialize()
            new_children.append(child_issue)
            child_issue_output = IssueOutput(child_issue)
            child_issue.handle_prompt_request(context, child_issue_output)
            child_issue._state = IssueState.ENTERED
            new_xmits, grand_children = self.process_issue_output(context, child_issue, child_issue_output)
            new_children.extend(grand_children)
            xmits.extend(new_xmits)
        return xmits, new_children

    def create_shadows(self, issue):
        shadows = issue.get_init_shadow_children(self.flexdiam_config)
        if shadows:
            for shadow in shadows:
                if isinstance(shadow, tuple):
                    self.log.trace(f"Create shadow {shadow.__name__!r} with config={shadow[1]}")
                    shadow_issue = shadow[0](abstract_name=shadow.__name__, parent=issue, config=shadow[1], as_shadow=True)
                else:
                    self.log.trace(f"Create shadow {shadow.__name__!r} with config={{}}")
                    shadow_issue = shadow(abstract_name=shadow.__name__, parent=issue, config={}, as_shadow=True)
                shadow_issue.initialize()
                issue.children.append(shadow_issue)

    def do_prompt_request(self, context, timeboard) -> list[XMit]:
        xmits, new_children = self.find_issue_for_prompt_request(context)
        self.active_issues.extend(new_children)
        return xmits

    def find_issue_for_prompt_request(self, context) -> tuple[list[XMit], list[Issue]]:
        new_children = []
        for issue in reversed(self.active_issues):
            if not issue.state.is_terminated():
                try:
                    issue_output = IssueOutput(issue)
                    if issue.state.is_in_setup():
                        issue._state = IssueState.ENTERED
                    if self.last_call_prompt_request_issue != issue.abstract_name:
                        self.log.info(f"Call prompt request {issue.abstract_name!r}")
                        self.last_call_prompt_request_issue = issue.abstract_name
                    issue.handle_prompt_request(context, issue_output)
                    return self.process_issue_output(context, issue, issue_output)
                    # or not? -> traverse other active issues based on issue output (no xmits?)
                except Exception:
                    self.log.error(traceback.format_exc())
                    issue._state = IssueState.FAILED
                    issue.invalidate()
                    # TODO add handle child closed
        return [], new_children
