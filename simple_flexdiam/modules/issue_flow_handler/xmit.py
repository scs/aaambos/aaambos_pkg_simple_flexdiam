from __future__ import annotations

from abc import abstractmethod
from enum import Enum
from typing import Type, TYPE_CHECKING

from attrs import field, define

if TYPE_CHECKING:
    from simple_flexdiam.modules.core.observer import Observer

# TODO move to core


class XMitState(Enum):
    NEW = "new"
    FULFILLED = "fulfilled"
    RETRACTED = "retracted"


@define(kw_only=True)
class XMit:
    state: XMitState = field(factory=lambda: XMitState.NEW)

    def retract(self):
        ...

    @classmethod
    @abstractmethod
    def get_observer_class(cls) -> Type[Observer]:
        ...