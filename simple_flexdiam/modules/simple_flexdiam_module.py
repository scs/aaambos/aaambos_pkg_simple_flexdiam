from __future__ import annotations

import re
from typing import Type, TYPE_CHECKING, Any

from aaambos.core.communication.service import CommunicationService
from aaambos.core.supervision.run_time_manager import ControlMsg
from aaambos.std.communications.attributes import MsgPayloadWrapperCallbackReceivingAttribute, MsgTopicSendingAttribute
from aaambos.std.extensions.module_status.module_status_extension import ModuleStatusExtensionFeature, \
    ModuleStatusInfoComFeatureOut, ModuleStatusesComFeatureIn
from attrs import define, field

from aaambos.core.configuration.module_config import ModuleConfig
from aaambos.core.module.base import Module, ModuleInfo
from aaambos.core.module.feature import FeatureNecessity, Feature, SimpleFeatureNecessity, FeatureName

from simple_flexdiam.modules.core.observer import ObserverSpace
from simple_flexdiam.modules.core.timeboard import TimeBoard

from simple_flexdiam.modules.core.features import ObserverReq, TierReq
if TYPE_CHECKING:
    from simple_flexdiam.modules.core.flow_handler import FlowHandler


class SimpleFlexdiam(Module, ModuleInfo):
    """A DialogManager based on FlexDiam. Consists of a Timeboard with tiers, observers that observe this tiers and a flow handler that controls the dialog flow. """
    config: SimpleFlexdiamConfig
    com: MsgTopicSendingAttribute | MsgPayloadWrapperCallbackReceivingAttribute | CommunicationService
    timeboard: TimeBoard
    flow_handler: FlowHandler
    observer: ObserverSpace

    def __init__(self, config: ModuleConfig, com: CommunicationService, log, ext, status,  *args,
                 **kwargs):
        super().__init__(config, com, log, ext, *args, **kwargs)
        self.status = status

    @classmethod
    def provides_features(cls, config: SimpleFlexdiamConfig = ...) -> dict[
        Feature.name, tuple[Feature, FeatureNecessity]]:
        features = {
            ModuleStatusInfoComFeatureOut.name: (ModuleStatusInfoComFeatureOut, SimpleFeatureNecessity.Required),
            ModuleStatusesComFeatureIn.name: (ModuleStatusesComFeatureIn, SimpleFeatureNecessity.Required),
        }
        flow_handler_features = config.flow_handler.flow_handler_class.provides_features(config)
        if flow_handler_features:
            features.update(flow_handler_features)
        for o_req in config.settings.observers:
            o_features = o_req.observer_class.provides_features(config, o_req.observer_config)
            if o_features:
                features.update(o_features)
        for t_req in config.settings.tiers:
            t_features = t_req.com_in_features.copy() + t_req.com_out_features
            if t_features:
                features.update({t.name: (t, SimpleFeatureNecessity.Required) for t in t_features})
        return features

    @classmethod
    def requires_features(cls, config: SimpleFlexdiamConfig = ...) -> dict[
        Feature.name, tuple[Feature, FeatureNecessity]]:
        features = {
            ModuleStatusExtensionFeature.name: (ModuleStatusExtensionFeature, SimpleFeatureNecessity.Required),
        }
        flow_handler_features = config.flow_handler.flow_handler_class.requires_features(config)
        if flow_handler_features:
            features.update(flow_handler_features)
        for o_req in config.settings.observers:
            o_features = o_req.observer_class.requires_features(config, o_req.observer_config)
            if o_features:
                features.update(o_features)
        for t_req in config.settings.tiers:
            if t_req.additional_requirements:
                features.update({t.name: (t, SimpleFeatureNecessity.Required) for t in t_req.additional_requirements})
        return features

    @classmethod
    def get_module_config_class(cls) -> Type[ModuleConfig]:
        return SimpleFlexdiamConfig

    async def initialize(self):
        self.flow_handler = self.config.flow_handler.flow_handler_class(self.log, self.ext, self.config)
        self.timeboard = TimeBoard(self.log, self.config, self.com, self.prm)
        await self.timeboard.initialize()
        self.observer = ObserverSpace(self.log, self.ext, self.config, self.timeboard,
                                      self.flow_handler.process_entry_point)
        for obs in self.observer.observers:
            for tier, obs_type in obs.get_tiers(config=self.config, observer_config=obs.observer_config).items():
                if isinstance(tier, TierReq):
                    await self.timeboard.register_observer(tier.tier_name, obs, obs_type)
                else:
                    for tier_name in self.timeboard.tiers.keys():
                        if re.match(tier, tier_name):
                            await self.timeboard.register_observer(tier_name, obs, obs_type)

        await self.flow_handler.initialize(self.observer, self.timeboard)
        await self.observer.initialize()
        await self.status.set_module_status("initialized", {"module": self.__class__.__name__})

    async def step(self):
        await self.observer.step()
        await self.flow_handler.step()
        await self.timeboard.step()

    def terminate(self, control_msg: ControlMsg = None) -> int:
        exit_code = super().terminate()
        return exit_code


@define(kw_only=True)
class FlowHandlerConfig:
    flow_handler_class: Type[FlowHandler]


@define(kw_only=True)
class SFConfigSettings:
    observers: list[ObserverReq]
    tiers: list[TierReq]


@define(kw_only=True)
class SimpleFlexdiamConfig(ModuleConfig):
    module_path: str = "simple_flexdiam.modules.simple_flexdiam_module"
    module_info: Type[ModuleInfo] = SimpleFlexdiam
    restart_after_failure: bool = True
    expected_start_up_time: float | int = 0
    mean_frequency_step: float = 24
    extra_observers: list[ObserverReq] = field(factory=list)
    flow_handler: FlowHandlerConfig
    tier_size: int = 100
    """Only store the last x events per tier"""
    gui: bool = True
    observer_config: dict[str, Any] = field(factory=dict)
    plus: dict[str, Any] = field(factory=dict)

    settings: SFConfigSettings | None = None

    def setup(self):
        """Sets the setting property based on observers from the flow handler. """
        super().setup()
        # convert from dict to attrs object
        self.flow_handler = self.flow_handler["flow_handler_class"].get_config_class(self)(**self.flow_handler)
        observer_reqs = self.flow_handler.flow_handler_class.observer_requirements(self)

        observer_reqs += self.extra_observers
        observer_feature_names = set()
        final_observers_with_config = []
        tier_names = set()
        tiers = []
        for o_req in observer_reqs:
            if o_req.observer_class.__name__ not in observer_feature_names:
                if o_req.observer_class.__name__ in self.observer_config:
                    # deep update?
                    o_req.observer_config.update(self.observer_config[o_req.observer_class.__name__])
                final_observers_with_config.append(o_req)
                for t_req in o_req.observer_class.get_tiers(self, o_req.observer_config):
                    if isinstance(t_req, TierReq) and t_req.tier_name not in tier_names:
                        tiers.append(t_req)
                        tier_names.add(t_req.tier_name)
                observer_feature_names.update([o_req.observer_class.__name__])

        self.settings = SFConfigSettings(observers=final_observers_with_config, tiers=tiers)


def provide_module():
    return SimpleFlexdiam
