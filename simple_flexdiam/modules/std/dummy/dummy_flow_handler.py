import uuid
from datetime import timedelta
from typing import Type

from aaambos.core.module.feature import Feature, FeatureNecessity

from simple_flexdiam.modules.core.definitions import AsrResultState, SpeechGenerationControl, Controls, NluResults
from simple_flexdiam.modules.core.features import ObserverReq
from simple_flexdiam.modules.core.flow_handler import FlowHandler
from simple_flexdiam.modules.core.observer import ObserverSpace
from simple_flexdiam.modules.simple_flexdiam_module import SimpleFlexdiamConfig, FlowHandlerConfig
from simple_flexdiam.modules.std.observers.issue_gui import issue_gui_obs_req
from simple_flexdiam.modules.std.observers.agent_words_observer import agent_words_observer_req
from simple_flexdiam.modules.std.observers.floor_observer import floor_observer_req
from simple_flexdiam.modules.std.observers.nlu_entry_point_observer import nlu_entry_point_observer_req
from simple_flexdiam.modules.std.observers.speech_generation_observer import speech_generation_observer_req, \
    SpeechGenerationObserver
from simple_flexdiam.modules.std.observers.user_words_observer import user_words_observer_req


class DummyFlowHandler(FlowHandler):
    speech_gen_obs: SpeechGenerationObserver
    async def process_entry_point(self, entry_point, data, timeboard):
        match entry_point:
            case NLU_PARSE_ENTRY_POINT:
                data: NluResults
                if data.state == AsrResultState.FINAL and self.speech_gen_obs:
                    speech_gen = SpeechGenerationControl(
                        control=Controls.START,
                        id=uuid.uuid4().hex,
                        text="Du sagtest:",
                    )
                    await self.speech_gen_obs.start_speech_generation(speech_gen, yield_time=timedelta(seconds=1))

                    self.log.info(f"Utterance received {data.utterance}")
                    speech_gen_2 = SpeechGenerationControl(
                        control=Controls.START,
                        id=uuid.uuid4().hex,
                        text=data.utterance,
                    )
                    await self.speech_gen_obs.start_speech_generation(speech_gen_2, yield_time=timedelta(seconds=1))

    async def initialize(self, observer: ObserverSpace, timeboard):
        await super().initialize(observer, timeboard)
        self.speech_gen_obs: SpeechGenerationObserver = self.observer.get_observer(SpeechGenerationObserver)


    @classmethod
    def provides_features(cls, config: SimpleFlexdiamConfig) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        return {}

    @classmethod
    def requires_features(cls, config: SimpleFlexdiamConfig) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        return {}

    @classmethod
    def observer_requirements(cls, config: SimpleFlexdiamConfig) -> list[ObserverReq]:
        return [
            agent_words_observer_req,
            floor_observer_req,
            nlu_entry_point_observer_req,
            speech_generation_observer_req,
            user_words_observer_req,
            issue_gui_obs_req,
        ]

    @classmethod
    def get_config_class(cls, config: SimpleFlexdiamConfig) -> Type[FlowHandlerConfig]:
        return FlowHandlerConfig