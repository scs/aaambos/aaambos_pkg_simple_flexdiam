import uuid
from datetime import timedelta
from typing import Type

from simple_flexdiam.modules.core.definitions import SpeechGenerationControl, Controls
from simple_flexdiam.modules.issue_flow_handler.issue import Issue, IssueOutput, IssueState
from simple_flexdiam.modules.issue_flow_handler.issue_flow_handler import Context
from simple_flexdiam.modules.issue_flow_handler.xmit import XMit
from simple_flexdiam.modules.simple_flexdiam_module import SimpleFlexdiamConfig
from simple_flexdiam.modules.std.issues.yes_no_question_issue import YNQIssue
from simple_flexdiam.modules.std.nlg.say import Say
from simple_flexdiam.modules.std.observers.nlu_entry_point_observer import NLU_PARSE_ENTRY_POINT
from simple_flexdiam.modules.std.xmits.nlg_xmit import NLGXMit
from simple_flexdiam.modules.std.xmits.speech_generation_xmit import SpeechGenerationXMit


class HowAreYouIssue(Issue):
    """Short dialog about, how are you. Trigger it with "wie geht es dir". """
    __slots__ = ()


    def initialize(self):
        self.locals.asked = False

    @classmethod
    def get_init_shadow_children(cls, config: SimpleFlexdiamConfig) -> list[Type[Issue] | tuple[Type[Issue], dict]]:
        pass

    @classmethod
    def potential_children_classes(cls, config: SimpleFlexdiamConfig) -> list[Type[Issue]]:
        return [YNQIssue]

    @classmethod
    def entry_points_of_interest(cls) -> list[str]:
        return [NLU_PARSE_ENTRY_POINT]

    @classmethod
    def potential_xmit_classes(cls) -> list[Type[XMit]]:
        return [NLGXMit]

    def handle_child_closed(self, child, context, output):
        if child.abstract_name == "needhelp":
            if child.locals.accepted:
                self.say("Vielleicht hilft es dir wenn du einen guten Freund anrufst. Über Probleme zu reden, ist ein guter Weg sie aus der Welt zu schaffen.", 0.0, output)
                self.set_end_state(IssueState.FULFILLED)
            else:
                self.say("Ok.", floor_yield=False, output=output)
                self.set_end_state(IssueState.FULFILLED)

    def can_handle_entry_point(self, entry_point, ep_data, context) -> bool:
        return entry_point == NLU_PARSE_ENTRY_POINT and (ep_data.intention in ["howareyou"] or (self.locals.asked and ep_data.intention in ["feelingwell", "feelingnotsowell"]))

    def handle_entry_point(self, entry_point, ep_data, context: Context, output: IssueOutput):
        if self.can_handle_entry_point(entry_point, ep_data, context):
            if ep_data.intention == "howareyou":
                self.say("Wenn ich dich sehe, geht es mir eigentlich immer gut! Und wie geht es dir?", 5.0, output)
                self.locals.asked = True
            if ep_data.intention == "feelingwell":
                self.say("Oh, das freut mich!", 5.0, output)
                self.set_end_state(IssueState.FULFILLED)
            if ep_data.intention == "feelingnotsowell":
                self.say("Das klingt aber nicht so gut.", 0.0, output)
                output.add_child(YNQIssue, "needhelp", config={"question": "Willst du darueber reden?"})

    def handle_prompt_request(self, context, output):
        self.say("Und wie geht es dir?", floor_yield=5.0, output=output)

    def say(self, text, floor_yield, output):
        output.add_xmit(NLGXMit(Say(text), floor_yield=floor_yield))
