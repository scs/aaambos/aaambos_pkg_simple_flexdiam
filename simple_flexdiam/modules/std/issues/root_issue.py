import uuid
from datetime import timedelta
from typing import Type

from simple_flexdiam.modules.core.definitions import SpeechGenerationControl, Controls
from simple_flexdiam.modules.issue_flow_handler.issue import Issue, IssueOutput
from simple_flexdiam.modules.issue_flow_handler.issue_fh_config import IssueFHConfig
from simple_flexdiam.modules.issue_flow_handler.issue_flow_handler import Context
from simple_flexdiam.modules.issue_flow_handler.xmit import XMit
from simple_flexdiam.modules.simple_flexdiam_module import SimpleFlexdiamConfig
from simple_flexdiam.modules.std.observers.nlu_entry_point_observer import NLU_PARSE_ENTRY_POINT
from simple_flexdiam.modules.std.xmits.speech_generation_xmit import SpeechGenerationXMit


class RootIssue(Issue):
    __slots__ = ()

    def initialize(self):
        pass

    @classmethod
    def get_init_shadow_children(cls, config: SimpleFlexdiamConfig) -> list[Type[Issue] | tuple[Type[Issue], dict]]:
        shadow_children = []
        for issue_ref in config.flow_handler.shadow_root_issues:
            if issubclass(issue_ref, Issue):
                shadow_children.append(issue_ref)
            elif isinstance(issue_ref, list):
                shadow_children.append(tuple(issue_ref))
            else:
                shadow_children.extend(issue_ref())

        return shadow_children

    @classmethod
    def potential_children_classes(cls, config: SimpleFlexdiamConfig) -> list[Type[Issue]]:
        return cls.get_init_shadow_children(config)

    @classmethod
    def potential_xmit_classes(cls) -> list[Type[XMit]]:
        return []

    def entry_points_of_interest(cls) -> list[str]:
        return []

    def handle_child_closed(self, child, context, output):
        pass

    def can_handle_entry_point(self, entry_point, ep_data, context) -> bool:
        return True

    def handle_entry_point(self, entry_point, ep_data, context: Context, output: IssueOutput):
        pass

    def handle_prompt_request(self, context, output):
        pass