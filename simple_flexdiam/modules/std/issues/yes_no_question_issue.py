import uuid
from datetime import timedelta
from typing import Type

from simple_flexdiam.modules.core.definitions import SpeechGenerationControl, Controls
from simple_flexdiam.modules.issue_flow_handler.issue import Issue, IssueOutput, IssueState
from simple_flexdiam.modules.issue_flow_handler.issue_flow_handler import Context
from simple_flexdiam.modules.issue_flow_handler.xmit import XMit
from simple_flexdiam.modules.simple_flexdiam_module import SimpleFlexdiamConfig
from simple_flexdiam.modules.std.nlg.say import Say
from simple_flexdiam.modules.std.observers.nlu_entry_point_observer import NLU_PARSE_ENTRY_POINT
from simple_flexdiam.modules.std.xmits.nlg_xmit import NLGXMit
from simple_flexdiam.modules.std.xmits.speech_generation_xmit import SpeechGenerationXMit


class YNQIssue(Issue):
    __slots__ = ()

    @classmethod
    def get_init_shadow_children(cls, config: SimpleFlexdiamConfig) -> list[Type[Issue] | tuple[Type[Issue], dict]]:
        pass

    @classmethod
    def potential_children_classes(cls, config: SimpleFlexdiamConfig) -> list[Type[Issue]]:
        pass

    @classmethod
    def entry_points_of_interest(cls) -> list[str]:
        return [NLU_PARSE_ENTRY_POINT]

    @classmethod
    def potential_xmit_classes(cls) -> list[Type[XMit]]:
        return [NLGXMit]

    def initialize(self):
        self.locals.accepted = False

    def handle_child_closed(self, child, context, output):
        pass

    def can_handle_entry_point(self, entry_point, ep_data, context) -> bool:
        return entry_point == NLU_PARSE_ENTRY_POINT and ep_data.intention in ["affirm", "deny"]

    def handle_entry_point(self, entry_point, ep_data, context: Context, output: IssueOutput):
        if self.can_handle_entry_point(entry_point, ep_data, context):
            self.locals.accepted = ep_data.intention == "affirm"
            self.set_end_state(IssueState.FULFILLED)

    def handle_prompt_request(self, context, output):
        output.add_xmit(NLGXMit(Say(self.config["question"]), floor_yield=5.0))
