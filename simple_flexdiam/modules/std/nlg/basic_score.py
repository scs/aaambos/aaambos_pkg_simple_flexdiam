from typing import Tuple

from simple_flexdiam.modules.core.nlg import NLGScore


class NLGScorePercent(NLGScore):

    def __init__(self, score_percent: float, /):
        self.score_percent = score_percent

    def get_score(self, other_behaviours, score_range: Tuple[float, float], observer_config: dict) -> float:
        return score_range[0] + (score_range[1] - score_range[0]) * self.score_percent


class NLGConfigScorePercent(NLGScore):

    def __init__(self, config_ref: str, /, default: float, *, obs_config_section="nlg_score_percent"):
        self.config_plus_section = obs_config_section
        self.config_ref = config_ref
        self.default = default

    def get_score(self, other_behaviours, score_range: Tuple[float, float], observer_config: dict) -> float:
        score_percent = self.default
        if self.config_plus_section in observer_config and self.config_ref in observer_config[self.config_plus_section]:
            score_percent = observer_config[self.config_plus_section]
        return score_range[0] + (score_range[1] - score_range[0]) * score_percent


class NLGMatchScorePercent(NLGScore):

    def __init__(self, match_item_frequency: float, similar_matches_frequency: float, /, default_min, default_max, balance: float = 0.5, *, obs_config_section="nlg_score_match"):
        self.match_item_frequency = match_item_frequency
        self.similar_matches_frequency = similar_matches_frequency
        self.balance = balance
        self.config_plus_section = obs_config_section
        self.range = (default_min, default_max)

    def get_score(self, other_scores: list["NLGScore"], score_range: Tuple[float, float], observer_config: dict) -> float:
        score_range_2 = self.range
        if self.config_plus_section in observer_config:
            config = observer_config[self.config_plus_section]
            if len(config) == 2:
                score_range_2 = config
            elif len(config) == 3:
                *score_range_2, self.balance = config
        match_score = score_range_2[0] + (score_range_2[1] - score_range_2[0]) * (self.match_item_frequency * (1-self.balance) + self.similar_matches_frequency * self.balance)
        return score_range[0] + (score_range[1] - score_range[0]) * match_score
