from __future__ import annotations

import json
import os
from pathlib import Path
from random import Random
from string import Formatter
from typing import Tuple, Any, Type, Set

import yaml

from simple_flexdiam.modules.core.nlg import NLGProducer, NLGRequest, NLGScore, NLGDelayedProducer, \
    register_nlg_behavior
from simple_flexdiam.modules.std.nlg.basic_score import NLGMatchScorePercent

from simple_flexdiam.modules.std.observers.natural_language_generation_observer import NLGObserverConfigType


class FillNLGObserverConfigType(NLGObserverConfigType):
    template_seed: int
    templates: str | Path


class FillNLGProducer(NLGProducer):

    def __init__(self, log, nlg_observer_config: FillNLGObserverConfigType, plus_config: dict[str, Any], *args,
                 **kwargs):
        super().__init__(log, nlg_observer_config, plus_config, *args, **kwargs)
        # self.safe_substitute = nlg_observer_config["template_safe_substitute"] if "template_safe_substitute" not in nlg_observer_config else True
        # for safe substitution find rule for not replaced variables: https://stackoverflow.com/questions/17215400/format-string-unused-named-arguments
        self.seed = nlg_observer_config["template_seed"] if "template_seed" in nlg_observer_config else 42
        self.random = Random(self.seed)
        template_file = nlg_observer_config["templates"] if "templates" in nlg_observer_config else None

        if not template_file:
            self.log.error("No template file passed to template nlg producer.")
            return
        if isinstance(template_file, dict):
            template_data = template_file
        else:
            if not os.path.isfile(template_file):
                self.log.error(f"No file at {template_file} for template nlg producer.")
                return
            template_data = None
            if str(template_file).endswith(".json"):
                with open(template_file, "r") as f:
                    template_data = json.load(f)
            elif str(template_file).endswith(".yaml") or str(template_file).endswith(".yml"):
                with open(template_file, "r") as f:
                    template_data = yaml.safe_load(f)
        if not template_data:
            self.log.error(
                f"Template file format not supported {template_file}. Should end with '.json' or '.yaml' or '.yml'")
            return
        self.templates = template_data
        self.template_info: dict[str, list[Tuple[int, Set[str]]]] = {}
        for template_key, templates in template_data.items():
            self.template_info[template_key] = [(len([r[1] for r in Formatter().parse(t) if r[1] == ""]),
                                                 set([r[1] for r in Formatter().parse(t) if r[1]])) for t in
                                                (templates if isinstance(templates, list) else [templates])]

    def can_produce(self, request: Fill) -> bool:
        return bool(self.templates) and isinstance(request,
                                                   Fill) and request.template_ref in self.template_info and any(
            [len(request.args) == t_info[0] and t_info[1].issubset(request.kwargs.keys()) for t_info in
             self.template_info[request.template_ref]])

    async def produce(self, request: Fill, score_only: bool = False, **kwargs) -> Tuple[Any, NLGScore]:
        possible_templates = []
        for t_info, template in zip(self.template_info[request.template_ref], self.templates[request.template_ref]):
            if len(request.args) == t_info[0] and t_info[1].issubset(request.kwargs.keys()):
                if len(request.kwargs.keys()) == 0:
                    relative_match = 1.0
                else:
                    relative_match = len(t_info[1].intersection(request.kwargs.keys())) / len(request.kwargs.keys())
                possible_templates.append((template, relative_match))

        # future option: include all possible templates even if they do not have the highest score?
        max_template = max(possible_templates, key=lambda x: x[1])
        are_best_templates = [x[1] == max_template[1] for x in possible_templates]
        score = NLGMatchScorePercent(max_template[1], len(are_best_templates) / len(possible_templates), 0.3, 0.7)
        if score_only:
            return [], score
        return self.random.choice(
            [t[0] for t, is_best in zip(possible_templates, are_best_templates) if is_best]).format(*request.args,
                                                                                                    **request.kwargs), score


@register_nlg_behavior
class Fill(NLGRequest):
    """Fill templates via string formatting.

     Template file contains template strings indexed by reference key. Every reference key can have a list of templates.
     The templates for a key can have different slot names. But a Fill request needs to match at least all slots for one template for that key.

     You can use the Fill request inside a NLGXMit like all NLGRequests. Refer to the template key and add your arguments/slots.

     The python formatter is used (`"my template {slot}".format(slot="123")`). So, you can add arguments in the format call that are not matched in the template.
     Positional arguments are `"{} {}".format(1,2) == "1 2"` (not recommended)

     Example:
         ```yaml
         acquaint:
           - "My name is {name}."
           - "My name is {name} {last_name}."
           - "I am {age} years old."
         greeting:
           - "Hello".
           - "Hello {name}!"
           - "Hey {name}!"
         ```

         Inside your Issue:
         ```python
         output.add(NLGXmit(Fill("acquaint", name="John")))
         output.add(NLGXmit(Fill("acquaint", name="John", last_name="Doe")))
         output.add(NLGXmit(Fill("acquaint", age=10)))
         # output.add(NLGXmit(Fill("acquaint")))  # not valid
         output.add(NLGXmit(Fill("greeting")))
         output.add(NLGXmit(Fill("greeting", name="Max")))  # random choice between "Hello Max!" and "Hey Max!"
         output.add(NLGXmit(Fill("greeting", name="Max", last_name="Doe")))  #  also valid even if last_name is not used.
         ```
        """

    def __init__(self, template_ref, *args, **kwargs):
        # add default_min, default_max, balance scoring kwargs? like _default_range, _balance ?
        self.template_ref = template_ref
        self.args = args
        self.kwargs = kwargs

    @classmethod
    def default_nlg_behavior_class(cls) -> Type[NLGProducer] | Type[NLGDelayedProducer]:
        return FillNLGProducer

    def __str__(self):
        return f"{self.__class__.__name__}({self.template_ref}, {self.args}, {self.kwargs})"
