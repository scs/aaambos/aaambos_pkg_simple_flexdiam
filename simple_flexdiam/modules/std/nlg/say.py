from typing import Tuple, Any, Type

from simple_flexdiam.modules.core.nlg import NLGProducer, NLGScore, NLGRequest, register_nlg_behavior, \
    NLGDelayedProducer
from simple_flexdiam.modules.std.nlg.basic_score import NLGConfigScorePercent


class BasicNLGProducer(NLGProducer):

    def can_produce(self, request: NLGRequest) -> bool:
        return hasattr(request, "utterance")

    async def produce(self, request: NLGRequest, score_only: bool = False, **kwargs) -> Tuple[Any, NLGScore]:
        assert hasattr(request, "utterance")
        return request.utterance, NLGConfigScorePercent("say", 0.5)


@register_nlg_behavior
class Say(NLGRequest):
    """Direct utterance generation.

    Example:
        Inside your issue:
        ```python
        output.add(NLGXmit(Say("Hello!")))
        output.add(NLGXmit(Say("How are you!")))
        output.add(NLGXmit(Say("Nice to meet you.")))
        ```
    """

    @classmethod
    def default_nlg_behavior_class(cls) -> Type[NLGProducer] | Type[NLGDelayedProducer]:
        return BasicNLGProducer

    def __init__(self, utterance, /):
        self.utterance = utterance

    def __str__(self):
        return f"{self.__class__.__name__}('{self.utterance}')"



