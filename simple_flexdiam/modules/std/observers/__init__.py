"""
Some basic observers for handling different input and output messages.

- AgentWordObserver: Based on SpeechGeneration control and status messages, it adds agent words interval events to the timeboard.
- FloorObserver: Sets the floor state to silence, agent, user, both (depends on who is speaking).
- IssueGUI:  Gui for issue flow handler.
- NluEntryPointObserver: Passes the received NLU messages to the flow handler.
- SpeechGenerationObserver: Based on the floor state and yielded events, it adds SpeechGeneration messages to the com service. (received from the flow handler).
- UserWordsObserver: Manages the user words interval events based on the asr results.

"""