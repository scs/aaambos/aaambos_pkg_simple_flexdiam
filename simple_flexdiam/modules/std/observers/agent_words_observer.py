from aaambos.core.module.feature import Feature, FeatureNecessity

from simple_flexdiam.modules.core.definitions import SpeechGenerationControl, SpeechGenerationStatus, Controls
from simple_flexdiam.modules.core.features import TierReq, ObserverReq
from simple_flexdiam.modules.core.observer import Observer
from simple_flexdiam.modules.core.tier import ObservationType, Tier, IntervalEvent, Event
from simple_flexdiam.modules.simple_flexdiam_module import SimpleFlexdiamConfig
from simple_flexdiam.modules.std.tiers.basic import SpeechGenerationTier, AgentWordsTier


class AgentWordsObserver(Observer):
    """Create `IntervalEvents` on the `AgentWordsTier` from the `SpeechGenerationControl` and `SpeechGenerationStatus` `PointEvents`.
    """
    last_agent_words = None

    @classmethod
    def provides_features(cls, config: SimpleFlexdiamConfig, observer_config: dict = ...) -> dict[
        Feature.name, tuple[Feature, FeatureNecessity]]:
        return {}

    @classmethod
    def requires_features(cls, config: SimpleFlexdiamConfig, observer_config: dict = ...) -> dict[
        Feature.name, tuple[Feature, FeatureNecessity]]:
        return {}

    @classmethod
    def get_tiers(cls, config: SimpleFlexdiamConfig, observer_config: dict) -> dict[TierReq, ObservationType | list[ObservationType]]:
        return {AgentWordsTier: ObservationType.WRITE, SpeechGenerationTier: [ObservationType.OBSERVE, ObservationType.WRITE]}

    async def initialize(self):
        await super().initialize()

    async def handle_new_event(self, tier: Tier, event: Event):
        data: SpeechGenerationStatus | SpeechGenerationControl = event.data
        if tier.name == SpeechGenerationTier.tier_name:
            # TODO check same ids
            if isinstance(data, SpeechGenerationControl):
                if self.last_agent_words is None and data.control == Controls.START:
                    self.last_agent_words = IntervalEvent(start_point=data.time, data={"control": data}, short_view=data.text)
                    await self.timeboard.add_event(AgentWordsTier.tier_name, self.last_agent_words)
            elif isinstance(data, SpeechGenerationStatus):
                if self.last_agent_words is None:
                    # should not happen
                    return
                if data.status.is_termination_status():
                    self.last_agent_words.end_point = data.time
                    self.last_agent_words.data.update({"status": data})
                    await self.timeboard.update_event(AgentWordsTier.tier_name, self.last_agent_words)
                    self.last_agent_words = None


agent_words_observer_req = ObserverReq(observer_class=AgentWordsObserver, observer_config={})
