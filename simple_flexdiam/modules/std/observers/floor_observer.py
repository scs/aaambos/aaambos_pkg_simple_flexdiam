from datetime import datetime
from enum import Enum

from aaambos.core.module.feature import Feature, FeatureNecessity

from simple_flexdiam.modules.core.features import TierReq, ObserverReq
from simple_flexdiam.modules.core.observer import Observer
from simple_flexdiam.modules.core.tier import ObservationType, Tier, IntervalEvent
from simple_flexdiam.modules.simple_flexdiam_module import SimpleFlexdiamConfig
from simple_flexdiam.modules.std.tiers.basic import AgentWordsTier, FloorTier, UserWordsTier


class FloorState(Enum):
    SILENCE = "silence"
    USER = "user"
    AGENT = "agent"
    OVERLAP = "overlap"


class FloorObserver(Observer):
    """Observe the UserWords- and AgentWordsTier and creates and updates the IntervalEvents on the FloorTier. These events are values from the `FloorState`."""
    last_floor_event = None
    @classmethod
    def provides_features(cls, config: SimpleFlexdiamConfig, observer_config: dict = ...) -> dict[
        Feature.name, tuple[Feature, FeatureNecessity]]:
        pass

    @classmethod
    def requires_features(cls, config: SimpleFlexdiamConfig, observer_config: dict = ...) -> dict[
        Feature.name, tuple[Feature, FeatureNecessity]]:
        pass

    @classmethod
    def get_tiers(cls, config: SimpleFlexdiamConfig, observer_config: dict) -> dict[TierReq, ObservationType | list[ObservationType]]:
        return {UserWordsTier: ObservationType.OBSERVE, FloorTier: ObservationType.WRITE, AgentWordsTier: ObservationType.OBSERVE}

    async def handle_new_event(self, tier: Tier, event, is_update=False):
        match self.last_floor_event.data:
            case FloorState.SILENCE:
                if tier.name == UserWordsTier.tier_name:
                    if event.end_point:
                        # until now: Silence and user ended speech
                        # should not happen
                        ...
                    elif not is_update:
                        # until now: Silence and user started speech
                        await self.change_state(new_state=FloorState.USER, time=event.start_point)
                elif tier.name == AgentWordsTier.tier_name:
                    if event.end_point:
                        # until now: Silence and agent ended speech
                        # should not happen
                        ...
                    elif not is_update:
                        # until now: Silence and agent started speech
                        await self.change_state(new_state=FloorState.AGENT, time=event.start_point)
            case FloorState.AGENT:
                if tier.name == UserWordsTier.tier_name:
                    if event.end_point:
                        # until now: Agent speaks and user ended speech
                        await self.change_state(new_state=FloorState.SILENCE, time=event.end_point)
                    elif not is_update:
                        # until now: Agent speaks and user started speech
                        await self.change_state(new_state=FloorState.OVERLAP, time=event.start_point)
                elif tier.name == AgentWordsTier.tier_name:
                    if event.end_point:
                        # until now: Agent speaks and agent ended speech
                        await self.change_state(new_state=FloorState.SILENCE, time=event.end_point)
                    elif not is_update:
                        # until now: Agent speaks and agent started speech
                        # should not happen
                        ...
            case FloorState.OVERLAP:
                if tier.name == UserWordsTier.tier_name:
                    if event.end_point:
                        # until now: Both speak and user ended speech
                        await self.change_state(new_state=FloorState.AGENT, time=event.end_point)
                    elif not is_update:
                        # until now: Both speak and user started speech
                        # should not happen
                        ...
                elif tier.name == AgentWordsTier.tier_name:
                    if event.end_point:
                        # until now: Both speak and agent ended speech
                        await self.change_state(new_state=FloorState.USER, time=event.end_point)
                    elif not is_update:
                        # until now: Both speak and agent started speech
                        # should not happen
                        ...
            case FloorState.USER:
                if tier.name == UserWordsTier.tier_name:
                    if event.end_point:
                        # until now: User speaks and user ended speech
                        await self.change_state(new_state=FloorState.SILENCE, time=event.end_point)
                    elif not is_update:
                        # until now: User speaks and user started speech
                        # should not happen
                        ...
                elif tier.name == AgentWordsTier.tier_name:
                    if event.end_point:
                        # until now: User speaks and agent ended speech
                        # should not happen
                        ...
                    elif not is_update:
                        # until now: User speaks and agent started speech
                        await self.change_state(new_state=FloorState.OVERLAP, time=event.start_point)

    async def handle_event_update(self, tier: Tier, event):
        await self.handle_new_event(tier, event, is_update=True)

    async def step(self):
        if not self.last_floor_event:
            self.last_floor_event = IntervalEvent(start_point=datetime.now(), data=FloorState.SILENCE, short_view=FloorState.SILENCE.value)
            await self.timeboard.tiers[FloorTier.tier_name].add_event(self.last_floor_event)

    async def change_state(self, new_state: FloorState, time: datetime):
        last_state = self.last_floor_event
        self.log.trace(f"Change floor from {self.last_floor_event.short_view} to {new_state.value}")
        self.last_floor_event = IntervalEvent(start_point=time, data=new_state,
                                              short_view=new_state.value)
        await self.timeboard.tiers[FloorTier.tier_name].add_event(self.last_floor_event)
        last_state.end_point = time
        await self.timeboard.tiers[FloorTier.tier_name].update_event(last_state)


floor_observer_req = ObserverReq(observer_class=FloorObserver, observer_config={})
