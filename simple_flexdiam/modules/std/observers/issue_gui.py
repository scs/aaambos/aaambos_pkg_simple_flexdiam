from __future__ import annotations

from collections import deque, namedtuple
from datetime import datetime, timedelta
from pprint import pprint
from typing import Any, TYPE_CHECKING

from PySimpleGUI import TEXT_LOCATION_BOTTOM_LEFT, TEXT_LOCATION_TOP, TEXT_LOCATION_LEFT, TEXT_LOCATION_TOP_LEFT
from aaambos.core.module.feature import Feature, FeatureNecessity, SimpleFeatureNecessity
from aaambos.std.guis.pysimplegui.pysimplegui_window_ext import PySimpleGUIWindowExtensionFeature, \
    PY_SIMPLE_GUI_WINDOW_EXTENSION
from aaambos.std.guis.pysimplegui.window_extension import PySimpleGUIWindowExtension

from simple_flexdiam.modules.core.features import TierReq, ObserverReq
from simple_flexdiam.modules.core.observer import Observer

import PySimpleGUI as sg

from simple_flexdiam.modules.core.tier import ObservationType, IntervalEvent, Tier, Event
from simple_flexdiam.modules.issue_flow_handler.issue import IssueState, Issue
from simple_flexdiam.modules.simple_flexdiam_module import SimpleFlexdiamConfig

if TYPE_CHECKING:
    from simple_flexdiam.modules.issue_flow_handler.issue_flow_handler import IssueFlowHandler

GUI_WIDTH = "gui_with"
TIER_HEIGHT = "tier_height"
TIER_LINE_START = "tier_line_start"
TIER_LINE_END_OFFSET = "tier_line_end_offset"
NOW_LINE_POS = "now_line_pos"
HORIZON = "horizon"

ISSUE_TREE_HEIGHT = "issue_tree_height"
ISSUE_BOX_WIDTH = "issue_box_width"
ISSUE_BOX_HEIGHT = "issue_box_height"
ISSUE_DISTANCE_HEIGHT = "issue_distance_height"
ISSUE_DISTANCE_WIDTH = "issue_distance_with"
ISSUE_START_X = "issue_start_x"
ISSUE_START_Y = "issue_start_y"
MAX_TREE_DEPTH = "max_tree_depth"
"""Does not guarantee to be visible"""
MAX_TREE_BREATH = "max_tree_breath"
"""Does not guarantee to be visible"""




ISSUE_FILL_COLOR_ACTIVE = "white"
"""colors see https://i0.wp.com/www.wikipython.com/wp-content/uploads/PySimpleGUI-hex-code-snapshot.png"""
ISSUE_FILL_COLOR_FULFILLED = "gold"
ISSUE_FILL_COLOR_FAILED = "DarkRed"
ISSUE_FILL_COLOR_OBSOLETE = "gray30"
ISSUE_FILL_COLOR_SHADOW = "LightBlue1"
ISSUE_FILL_COLOR_SETUP = "PaleGreen3"
ISSUE_COLOR_FALLBACK = "dark khaki"
GRAPH_BACKGROUND_COLOR = "graph_background_color"


IssuePos = namedtuple("IssuePos", ["l_c", "l_t", "r_c", "r_b"])
"""store the positions of coordinates of issues, left_center, left_top, right_center, right_back"""

root_wrapper = namedtuple("root_wrapper", "children")


def set_pos_and_find_depth(tree, depth, down, pos, parents_pos, children_func, child_access, parent_pos=(0,0)):
    if not children_func(tree):
        pos.append([])
        parents_pos.append([])
        return 1, depth
    cur_depth = depth + 1
    old_down = down
    depths = [depth]

    for i, node in enumerate(children_func(tree)):
        if i > 0:
            pos[down].extend([None for _ in range(depth)])
            parents_pos[down].extend([None for _ in range(depth)])
        node_pos = down, len(pos[down])
        pos[down].append(node)
        parents_pos[down].append(parent_pos)
        new_down, new_depth = set_pos_and_find_depth(child_access(tree, node), cur_depth, down, pos, parents_pos, children_func, child_access, node_pos)
        down += new_down
        depths.append(new_depth)

    return down - old_down, max(depths)


class IssueGUIObserver(Observer):
    """Visualize the `Tier`s and `Issue`s in a GUI."""
    flow_handler: IssueFlowHandler
    gui_window: PySimpleGUIWindowExtension
    last_now: datetime
    event_ids_to_graph_obj: dict[str, list[Any]]
    event_ids_to_time: dict[str, tuple[datetime, datetime | None]]
    event_ids_to_interval_event: dict[str, tuple[IntervalEvent, int]]
    new_events: list[(int, Event)]
    tier_name_to_tier_idx: dict[str, int]
    graph_obj_to_issue = dict[Any, Issue]
    other_issue_graph_objs_to_delete = list[Any]

    @classmethod
    def provides_features(cls, config: SimpleFlexdiamConfig, observer_config: dict = ...) -> dict[
        Feature.name, tuple[Feature, FeatureNecessity]]:
        return {}

    @classmethod
    def requires_features(cls, config: SimpleFlexdiamConfig, observer_config: dict = ...) -> dict[
        Feature.name, tuple[Feature, FeatureNecessity]]:
        return {
            PySimpleGUIWindowExtensionFeature.name: (
            PySimpleGUIWindowExtensionFeature, SimpleFeatureNecessity.Required),
        }

    @classmethod
    def get_tiers(cls, config: SimpleFlexdiamConfig, observer_config: dict) -> dict[TierReq, ObservationType | list[
        ObservationType]] | dict[str, ObservationType | list[ObservationType]]:
        # register all tiers
        return {'.*': ObservationType.OBSERVE}

    async def initialize(self):
        self.last_now = datetime.now()
        self.event_ids_to_graph_obj = {}
        self.tier_name_to_tier_idx = {}
        self.new_events = []
        self.event_ids_to_time = {}
        self.event_ids_to_interval_event = {}
        self.graph_obj_to_issue = {}
        self.other_issue_graph_objs_to_delete = []
        config = {
            GUI_WIDTH: 1200,
            TIER_HEIGHT: 50,
            TIER_LINE_START: 100,
            TIER_LINE_END_OFFSET: 20,
            NOW_LINE_POS: 950,
            HORIZON: 30,
            ISSUE_TREE_HEIGHT: 500,
            ISSUE_BOX_WIDTH: 150,
            ISSUE_BOX_HEIGHT: 10,
            ISSUE_DISTANCE_WIDTH: 30,
            ISSUE_DISTANCE_HEIGHT: 30,
            ISSUE_START_X: 20,
            ISSUE_START_Y: 20,
            MAX_TREE_DEPTH: 100,
            MAX_TREE_BREATH: 100,
            GRAPH_BACKGROUND_COLOR: "steel blue",
        }
        config.update(self.observer_config)
        self.observer_config = config
        self.gui_window = self.ext[PY_SIMPLE_GUI_WINDOW_EXTENSION]
        self.gui_window.set_event_handler(self.handle_window_event)

        self.gui_window.setup_window(window_title="SimpleFlexdiam", layout=self.create_layout())
        self.gui_window.window.finalize()
        self.setup_tier_graph()

    def create_layout(self):
        self.ntiers = len(self.timeboard.tiers)
        self.tier_graph_height = self.observer_config[TIER_HEIGHT] * self.ntiers + 30
        self.tier_line_y_offset = (self.observer_config[TIER_HEIGHT] / 2)
        self.one_second_width = (self.observer_config[NOW_LINE_POS] - self.observer_config[TIER_LINE_START]) / \
                                self.observer_config[HORIZON]
        self.tier_graph = sg.Graph(canvas_size=(self.observer_config[GUI_WIDTH], self.tier_graph_height),
                                   graph_bottom_left=(0, 0),
                                   graph_top_right=(self.observer_config[GUI_WIDTH], self.tier_graph_height),
                                   background_color=self.observer_config[GRAPH_BACKGROUND_COLOR], enable_events=True, key='tier_graph')
        self.issue_graph = sg.Graph(canvas_size=(self.observer_config[GUI_WIDTH], self.observer_config[ISSUE_TREE_HEIGHT]),
                                    graph_bottom_left=(0, 0),
                                    graph_top_right=(self.observer_config[GUI_WIDTH], self.observer_config[ISSUE_TREE_HEIGHT]),
                                    background_color=self.observer_config[GRAPH_BACKGROUND_COLOR], enable_events=True, key='issue_graph')
        self.half_issue_height = self.observer_config[ISSUE_BOX_HEIGHT]
        self.issue_plus_margin_x = self.observer_config[ISSUE_BOX_WIDTH] + self.observer_config[ISSUE_DISTANCE_WIDTH]
        self.issue_plus_margin_y = self.observer_config[ISSUE_BOX_HEIGHT] + self.observer_config[ISSUE_DISTANCE_HEIGHT]
        self.issue_positions = self.calc_issue_positions()
        return [
            [sg.T(f"TimeBoard with {self.ntiers} tiers", background_color="gray")],
            [self.tier_graph],
            [sg.T(f"Issue tree", background_color="gray")],
            [sg.Column([[self.issue_graph]], scrollable=True, vertical_scroll_only=True, size_subsample_height=max(1, (self.observer_config[ISSUE_TREE_HEIGHT])/(800 - self.tier_graph_height)))],
        ]

    def calc_issue_positions(self):
        issue_pos = []
        for r in range(self.observer_config[MAX_TREE_BREATH]):
            node_pos = []
            for n in range(self.observer_config[MAX_TREE_DEPTH]):
                node_pos.append(IssuePos(
                    l_c=(n * self.issue_plus_margin_x + self.observer_config[ISSUE_START_X], (self.observer_config[ISSUE_TREE_HEIGHT] - self.observer_config[ISSUE_START_Y]) - (r * self.issue_plus_margin_y)),
                    l_t=(n * self.issue_plus_margin_x + self.observer_config[ISSUE_START_X], (self.observer_config[ISSUE_TREE_HEIGHT] - self.observer_config[ISSUE_START_Y]) - (r * self.issue_plus_margin_y) + self.half_issue_height),
                    r_c=(n * self.issue_plus_margin_x + self.observer_config[ISSUE_START_X] + self.observer_config[ISSUE_BOX_WIDTH], (self.observer_config[ISSUE_TREE_HEIGHT] - self.observer_config[ISSUE_START_Y]) - (r * self.issue_plus_margin_y)),
                    r_b=(n * self.issue_plus_margin_x + self.observer_config[ISSUE_START_X] + self.observer_config[ISSUE_BOX_WIDTH], (self.observer_config[ISSUE_TREE_HEIGHT] - self.observer_config[ISSUE_START_Y]) - (r * self.issue_plus_margin_y) - self.half_issue_height),
                ))
            issue_pos.append(node_pos)
        return issue_pos

    async def handle_new_event(self, tier: Tier, event):
        self.new_events.append((self.tier_name_to_tier_idx[tier.name], event))

    async def handle_event_update(self, tier: Tier, event):
        if isinstance(event, IntervalEvent):
            if event.id in self.event_ids_to_graph_obj:
                left_pos = (self.calc_pos(datetime.now(), event.start_point),
                            self.tier_name_to_tier_idx[tier.name] * self.observer_config[
                                TIER_HEIGHT] + self.tier_line_y_offset + 5)
                text_id = self.tier_graph.draw_text(
                    " " + (event.short_view if event.short_view else event.data.__class__.__name__), left_pos,
                    text_location=TEXT_LOCATION_TOP_LEFT)
                self.tier_graph.delete_figure(self.event_ids_to_graph_obj[event.id][1])
                self.event_ids_to_graph_obj[event.id][1] = text_id
                # TODO reset rectangle to correct end
                if self.event_ids_to_time[event.id][1] is not None and self.event_ids_to_time[event.id][1] != event.end_point:
                    self.tier_graph.delete_figure(self.event_ids_to_graph_obj[event.id][0])
                    obj_id = self.tier_graph.draw_rectangle(
                        top_left=left_pos,
                        bottom_right=(self.calc_pos(datetime.now(), event.end_point), self.event_ids_to_interval_event[event.id][1] * self.observer_config[TIER_HEIGHT] + self.tier_line_y_offset - 15),
                        fill_color="white")
                    self.event_ids_to_graph_obj[event.id][0] = obj_id
                self.event_ids_to_time[event.id] = (event.start_point, event.end_point)


    async def handle_window_event(self, event, values):
        if event == 'issue_graph':
            for x in self.issue_graph.get_figures_at_location(values[event]):
                if x in self.graph_obj_to_issue:
                    issue = self.graph_obj_to_issue[x]
                    pop_up = [f"Abstract Name: {issue.abstract_name}",f"State: {issue.state.name}", "Locals:"]
                    locals = [ f"{l} = {getattr(issue.locals, l)}" for l in dir(issue.locals) if not l.startswith("__") ]
                    pop_up.extend(locals)
                    sg.popup("\n".join(pop_up), title=issue.abstract_name, non_blocking=True)

    def setup_tier_graph(self):
        for i, tier in enumerate(reversed(self.timeboard.tiers)):
            self.tier_name_to_tier_idx[tier] = i
            self.tier_graph.draw_text(text=tier, location=(
            10, i * self.observer_config[TIER_HEIGHT] + self.tier_line_y_offset - 6),
                                      text_location=TEXT_LOCATION_BOTTOM_LEFT)
            self.tier_graph.draw_line(point_from=(
            self.observer_config[TIER_LINE_START], i * self.observer_config[TIER_HEIGHT] + self.tier_line_y_offset),
                                      point_to=(
                                      self.observer_config[GUI_WIDTH] - self.observer_config[TIER_LINE_END_OFFSET],
                                      i * self.observer_config[TIER_HEIGHT] + self.tier_line_y_offset))

        self.tier_graph.draw_line(point_from=(self.observer_config[NOW_LINE_POS], self.tier_graph_height - 30),
                                  point_to=(self.observer_config[NOW_LINE_POS], 0), color="darkgreen", width=3)
        self.tier_graph.draw_text(text="now",
                                  location=(self.observer_config[NOW_LINE_POS], self.tier_graph_height - 10),
                                  text_location=TEXT_LOCATION_TOP)

        skip = self.observer_config[HORIZON] if self.observer_config[HORIZON] < 30 else int(
            self.observer_config[HORIZON] / 15)
        now = datetime.now()
        for s in range(1, self.observer_config[HORIZON]):
            if s % skip:
                self.tier_graph.draw_text(f"-{s}s",
                                          (self.calc_pos(now, now - timedelta(seconds=s)), self.tier_graph_height - 10),
                                          text_location=TEXT_LOCATION_TOP)
        self.time_location = (self.observer_config[GUI_WIDTH] - 50,  self.tier_graph_height-10)
        self.time_obj = self.tier_graph.draw_text(now.strftime('%H:%M:%S.%f')[:-3], self.time_location)

    def calc_pos(self, now: datetime, point: datetime):
        return int((self.observer_config[NOW_LINE_POS] - ((now - point).total_seconds() * self.one_second_width)))

    @staticmethod
    def get_issue_color(state: IssueState):
        match state:
            case IssueState.FULFILLED:
                return ISSUE_FILL_COLOR_FULFILLED
            case IssueState.ENTERED:
                return ISSUE_FILL_COLOR_ACTIVE
            case IssueState.SHADOW:
                return ISSUE_FILL_COLOR_SHADOW
            case IssueState.NEW | IssueState.INVOKED:
                return ISSUE_FILL_COLOR_SETUP
            case IssueState.FAILED:
                return ISSUE_FILL_COLOR_FAILED
            case IssueState.OBSOLETE:
                return ISSUE_FILL_COLOR_OBSOLETE
        return ISSUE_COLOR_FALLBACK

    async def step(self):
        now = datetime.now()
        # diff = (self.last_now - now).total_seconds()
        self.last_now = now
        self.tier_graph.delete_figure(self.time_obj)
        self.time_obj = self.tier_graph.draw_text(now.strftime('%H:%M:%S.%f')[:-3], self.time_location)

        # offset = int(diff * self.one_second_width)
        del_line = (now - timedelta(seconds=self.observer_config[HORIZON]))

        events_to_delete = []
        # normal delta update
        for event_id, obj_ids in self.event_ids_to_graph_obj.items():
            self.tier_graph.relocate_figure(obj_ids[1], self.calc_pos(now, self.event_ids_to_time[event_id][0]) if self.event_ids_to_time[event_id][0] > del_line else self.observer_config[TIER_LINE_START],
                                            self.event_ids_to_interval_event[event_id][1] * self.observer_config[TIER_HEIGHT] + self.tier_line_y_offset  + 6)
            if self.event_ids_to_time[event_id][1] is None:
                # no endtime
                _, tier_idx = self.event_ids_to_interval_event[event_id]
                left_pos = (self.calc_pos(now,  self.event_ids_to_time[event_id][0]) if self.event_ids_to_time[event_id][0] > del_line else self.observer_config[TIER_LINE_START],
                            tier_idx * self.observer_config[TIER_HEIGHT] + self.tier_line_y_offset + 10)
                obj_id = self.tier_graph.draw_rectangle(top_left=left_pos, bottom_right=(
                self.observer_config[NOW_LINE_POS],
                tier_idx * self.observer_config[TIER_HEIGHT] + self.tier_line_y_offset - 10), fill_color="white")
                self.tier_graph.delete_figure(self.event_ids_to_graph_obj[event_id][0])
                self.event_ids_to_graph_obj[event_id][0] = obj_id
            elif self.event_ids_to_time[event_id][1] < del_line:
                # delete item if out of range
                events_to_delete.append(event_id)
                for obj_id in obj_ids:
                    self.tier_graph.delete_figure(obj_id)
            elif self.event_ids_to_time[event_id][0] < del_line:
                event, tier_idx = self.event_ids_to_interval_event[event_id]
                left_pos = (self.observer_config[TIER_LINE_START],
                            tier_idx * self.observer_config[TIER_HEIGHT] + self.tier_line_y_offset + 10)
                obj_id = self.tier_graph.draw_rectangle(top_left=left_pos, bottom_right=(
                self.calc_pos(now, event.end_point),
                tier_idx * self.observer_config[TIER_HEIGHT] + self.tier_line_y_offset - 10), fill_color="white")
                self.tier_graph.delete_figure(self.event_ids_to_graph_obj[event_id][0])
                self.event_ids_to_graph_obj[event_id][0] = obj_id
            else:
                self.tier_graph.relocate_figure(obj_ids[0], self.calc_pos(now, self.event_ids_to_time[event_id][0]),
                                                self.event_ids_to_interval_event[event_id][1] * self.observer_config[
                                                    TIER_HEIGHT] + self.tier_line_y_offset + 10)
            self.tier_graph.bring_figure_to_front(obj_ids[1])

        for event_id in events_to_delete:
            del self.event_ids_to_graph_obj[event_id]
            del self.event_ids_to_time[event_id]
            del self.event_ids_to_interval_event[event_id]

        for tier_idx, event in self.new_events:
            if isinstance(event, IntervalEvent):
                left_pos = (self.calc_pos(now, event.start_point),
                            tier_idx * self.observer_config[TIER_HEIGHT] + self.tier_line_y_offset + 10)
                obj_id = self.tier_graph.draw_rectangle(top_left=left_pos, bottom_right=((
                self.calc_pos(now, event.end_point) if event.end_point else self.observer_config[NOW_LINE_POS],
                tier_idx * self.observer_config[TIER_HEIGHT] + self.tier_line_y_offset - 10)), fill_color="white")
                text_id = self.tier_graph.draw_text(
                    " " + (event.short_view if event.short_view else event.data.__class__.__name__), (left_pos[0], left_pos[1]-5),
                    text_location=TEXT_LOCATION_TOP_LEFT)
                end_point = event.end_point
                start_point = event.start_point
                self.event_ids_to_interval_event[event.id] = (event, tier_idx)
            else:
                x_pos = self.calc_pos(now, event.time_point)
                tier_y_pos = tier_idx * self.observer_config[TIER_HEIGHT] + self.tier_line_y_offset
                obj_id = self.tier_graph.draw_line(point_from=(x_pos, tier_y_pos + 10),
                                                   point_to=(x_pos, tier_y_pos - 10))
                text_id = self.tier_graph.draw_text(
                    " " + (event.short_view if event.short_view else event.data.__class__.__name__), (x_pos, tier_y_pos + 5))
                end_point = event.time_point
                start_point = event.time_point
                self.event_ids_to_interval_event[event.id] = (event, tier_idx)
            self.event_ids_to_graph_obj[event.id] = [obj_id, text_id]
            self.event_ids_to_time[event.id] = (start_point, end_point)

        self.new_events = []

        if self.flow_handler.ep_finder.update_issue_view():
            for obj_id in self.graph_obj_to_issue:
                self.issue_graph.delete_figure(obj_id)
            for obj_id in self.other_issue_graph_objs_to_delete:
                self.issue_graph.delete_figure(obj_id)
            self.graph_obj_to_issue.clear()
            self.other_issue_graph_objs_to_delete = []
            pos = [[]]
            parents_pos: list[list[tuple[int, int]]] = [[]]
            down, depth = set_pos_and_find_depth(root_wrapper(children=[self.flow_handler.root_issue]), 0, 0, pos, parents_pos, lambda x: x.children, lambda t, n: n)
            for r, row in enumerate(pos):
                if r > down or r > self.observer_config[MAX_TREE_BREATH]:
                    break
                for n, node in enumerate(row):
                    if n > self.observer_config[MAX_TREE_DEPTH]:
                        break
                    if node is None:
                        continue

                    issue_pos = self.issue_positions[r][n]
                    pp_idx = parents_pos[r][n]
                    parent_pos = self.issue_positions[pp_idx[0]][pp_idx[1]]
                    obj_id = self.issue_graph.draw_rectangle(top_left=issue_pos.l_t, bottom_right=issue_pos.r_b, fill_color=self.get_issue_color(node.state))
                    self.other_issue_graph_objs_to_delete.append(self.issue_graph.draw_text(text=" " + node.abstract_name, location=issue_pos.l_c, text_location=TEXT_LOCATION_LEFT))
                    self.graph_obj_to_issue[obj_id] = node
                    if not (r == 0 and n == 0):
                        self.other_issue_graph_objs_to_delete.append(self.issue_graph.draw_line(parent_pos.r_c, issue_pos.l_c))


if __name__ == "__main__":
    graph = sg.Graph(canvas_size=(1200, 280), graph_bottom_left=(0, 0), graph_top_right=(1200, 280),
                     background_color='yellow', enable_events=True, drag_submits=True, key='graph')
    layout = [[graph], [sg.Button('LEFT'), sg.Button('RIGHT'), sg.Button('UP'), sg.Button('DOWN')]]
    window = sg.Window('Graph test', layout, finalize=True)
    floors = ["fllor", "asd", "asdsad", "asdasd", "asdasd"]
    for i, tier in enumerate(floors):
        graph.draw_text(text=tier, location=(10, i * 50 + 10),
                        text_location=TEXT_LOCATION_BOTTOM_LEFT, font=("gothic", 12, "italic"))
        graph.draw_line(point_from=(100, i * 50 + 25),
                        point_to=((1200 - 20,
                                   i * 50 + 25)))

    graph.draw_line((950, 250), (950, 0), color="darkgreen", width=3)
    graph.draw_text("now", (950, 280 - 10), text_location=TEXT_LOCATION_TOP)

    x1, y1 = 350, 150
    circle = graph.draw_circle((x1, y1), 10, fill_color='black', line_color='white')
    rectangle = graph.draw_rectangle((50, 50), (650, 250), line_color='purple')

    tree = sg.Graph(canvas_size=(1200, 280), graph_bottom_left=(0, 0), graph_top_right=(1200, 280),
                    background_color='gray', enable_events=True, drag_submits=True, key='graph')

    structure = {"root": {"x": {"y": None,
                                "z": {"a": None,
                                      "b": None},
                                "c": None},
                          "p": {"u": {"f": None},
                                "w": {"ö": None,
                                      "ä": {"t": {"l": None,
                                                  "x": None}}}}}}


    pos = [[]]
    parents_pos = [[]]
    assert len(structure) == 1, "only one root node"
    down, depth = set_pos_and_find_depth(structure, 0, 0, pos, parents_pos, lambda x: x, lambda t, n: t[n])

    pprint(pos)

    while True:
        event, values = window.read()
        if event == sg.WIN_CLOSED:
            break
        if event == 'RIGHT':
            graph.MoveFigure(circle, 10, 0)
        if event == 'LEFT':
            graph.MoveFigure(circle, -10, 0)
        if event == 'UP':
            graph.MoveFigure(circle, 0, 10)
        if event == 'DOWN':
            graph.MoveFigure(circle, 0, -10)
        if event == "graph+UP":
            x2, y2 = values['graph']
            graph.MoveFigure(circle, x2 - x1, y2 - y1)
            x1, y1 = x2, y2
    window.close()

issue_gui_obs_req = ObserverReq(observer_class=IssueGUIObserver, observer_config={})
