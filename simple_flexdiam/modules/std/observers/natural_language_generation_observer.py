from __future__ import annotations
import uuid
from abc import abstractmethod
from datetime import timedelta, datetime
from typing import Type, Tuple, TypedDict, TYPE_CHECKING

from aaambos.core.module.feature import Feature, FeatureNecessity

from simple_flexdiam.modules.core.definitions import SpeechGenerationControl, Controls
from simple_flexdiam.modules.core.features import TierReq, ObserverReq
from simple_flexdiam.modules.core.nlg import NLGProducer, NLGDelayedProducer, registered_nlg_request_types, NLGScore, \
    NLGRequest
from simple_flexdiam.modules.core.observer import Observer
from simple_flexdiam.modules.core.tier import ObservationType, Tier, IntervalEvent, Event, PointEvent
from simple_flexdiam.modules.core.timeboard import TimeBoard
from simple_flexdiam.modules.issue_flow_handler.xmit import XMit
from simple_flexdiam.modules.simple_flexdiam_module import SimpleFlexdiamConfig
from simple_flexdiam.modules.std.tiers.basic import NLGTier

if TYPE_CHECKING:
    from simple_flexdiam.modules.std.xmits.nlg_xmit import NLGXMit
from simple_flexdiam.modules.std.xmits.speech_generation_xmit import SpeechGenerationXMit


def default_producer_selector(scores: list[NLGScore], capable_producer: list[NLGProducer], score_range: tuple[float, float], observer_config: dict, *args, **kwargs) -> NLGProducer | NLGDelayedProducer:
    score_values = []
    for i, score in enumerate(scores):
        score_values.append(score.get_score(scores[:i] + scores[i + 1:], score_range, observer_config))
    final_producer = capable_producer[score_values.index(max(score_values))]
    return final_producer


class ProducerSelectorType:

    @abstractmethod
    def __call__(self, scores: list[NLGScore], capable_producer: list[NLGProducer], score_range: tuple[float, float], observer_config: dict, *args, **kwargs) -> NLGProducer | NLGDelayedProducer:
        ...


class NLGObserverConfigType(TypedDict):
    score_range: tuple[float, float]
    producer_selector: ProducerSelectorType
    ignore_nlg_producer: list[str]
    """Names of NLGProducer classes."""
    nlg_producer: list[Type[NLGProducer]]
    """Additional NLGProducer classes besides the default ones from the used NLGRequest classes."""


class NLGObserver(Observer):
    observer_config: NLGObserverConfigType

    def __init__(self, log, ext, process_entry_point, timeboard: TimeBoard, observer_config: dict):
        super().__init__(log, ext, process_entry_point, timeboard, observer_config)
        self.producer_repertoire: dict[str, NLGProducer | NLGDelayedProducer] = {}
        self.score_range = self.observer_config["score_range"] if "score_range" in self.observer_config.keys() else [0.0, 1.0]
        self.producer_selector: ProducerSelectorType = self.observer_config["producer_selector"] if "producer_selector" in self.observer_config.keys() else default_producer_selector
        self.current_delayed_producer: list[Tuple[NLGDelayedProducer, NLGXMit]] = []

    @classmethod
    def provides_features(cls, config: SimpleFlexdiamConfig, observer_config: dict = ...) -> dict[
        Feature.name, tuple[Feature, FeatureNecessity]]:
        return {}

    @classmethod
    def requires_features(cls, config: SimpleFlexdiamConfig, observer_config: dict = ...) -> dict[
        Feature.name, tuple[Feature, FeatureNecessity]]:
        return {}

    @classmethod
    def potential_xmit_classes(cls) -> list[Type[XMit]]:
        return [SpeechGenerationXMit]

    @classmethod
    def get_tiers(cls, config: SimpleFlexdiamConfig, observer_config: dict) -> dict[TierReq, ObservationType | list[ObservationType]]:
        # define here the tiers the observer wants to read, write, and observe (observe -> handle_new_event is called)
        # the tier already handles the com (In/Out) features
        tiers = {NLGTier: [ObservationType.WRITE]}
        delayed_producer: list[Type[NLGDelayedProducer]] = []
        for schedule_behavior_class in registered_nlg_request_types.values():
            # update observation types of tiers
            if issubclass(schedule_behavior_class.default_nlg_behavior_class(), NLGDelayedProducer):
                delayed_producer.append(schedule_behavior_class.default_nlg_behavior_class())
        if "nlg_producer" in observer_config:
            for producer in observer_config["nlg_producer"]:
                if issubclass(producer, NLGDelayedProducer):
                    delayed_producer.append(producer)

        for producer in delayed_producer:
            for t, obs_type in producer.get_tiers(config, observer_config).items():
                if t in tiers:
                    if isinstance(obs_type, list):
                        for o in obs_type:
                            if o not in tiers[t]:
                                tiers[t].append(o)
                    else:
                        if obs_type in tiers[t]:
                            tiers[t].append(obs_type)
                else:
                    tiers[t] = obs_type if isinstance(obs_type, list) else [obs_type]
        return tiers

    async def initialize(self):
        # gather all producer
        for behavior_class in registered_nlg_request_types.values():
            behavior_class.plus_config = self.flow_handler.flexdiam_config.plus

        ignore_producer = self.observer_config["ignore_nlg_producer"] if "ignore_nlg_producer" in self.observer_config else []
        for schedule_behavior_class in registered_nlg_request_types.values():
            default_producer = schedule_behavior_class.default_nlg_behavior_class()
            if default_producer.__name__ not in self.producer_repertoire and default_producer.__name__ not in ignore_producer:
                self.producer_repertoire[default_producer.__name__] = default_producer(self.log, self.observer_config, self.flow_handler.flexdiam_config.plus)
        if "nlg_producer" in self.observer_config:
            for producer in self.observer_config["nlg_producer"]:
                if producer.__name__ not in self.producer_repertoire:
                    self.producer_repertoire[producer.__name__] = producer(self.log, self.observer_config, self.flow_handler.flexdiam_config.plus)
        self.log.info(f"NLG Repertoire: {self.producer_repertoire}")
        await super().initialize()

    @staticmethod
    def prepare_new_xmits(utterance, xmit) -> list[XMit]:
        return [SpeechGenerationXMit(
            speech_generation=SpeechGenerationControl(
                text=utterance,
                id=uuid.uuid4(),
                control=Controls.START),
            floor_yield=timedelta(seconds=xmit.floor_yield)
        )]

    async def add_xmit(self, xmit):
        # maybe only do one at a time? (queue them?)

        # new nlg requests
        nlg_request: NLGRequest = xmit.nlg_request
        # find producers that can produce utterance for nlg request
        capable_producer = []
        await self.timeboard.tiers[NLGTier.tier_name].add_event(PointEvent(time_point=datetime.now(), data=nlg_request, short_view=f"{nlg_request})"))
        for producer in self.producer_repertoire.values():
            if producer.can_produce(nlg_request):
                capable_producer.append(producer)
        if not capable_producer:
            self.log.error(f"Cannot produce utterance with any NLGProducer nlg_request={nlg_request} repertoire={self.producer_repertoire}")
            return
        # let the producer estimate their score for the utterance to find the best utterance producer
        scores = []
        for producer in capable_producer:
            if isinstance(producer, NLGProducer):
                _, score = await producer.produce(nlg_request, score_only=True)
            else:
                _, score = await producer.prepare(nlg_request, score_only=True)
            scores.append(score)
        # let the producer selector function decide which producer won.
        final_producer = self.producer_selector(scores, capable_producer, self.score_range, self.observer_config)
        if isinstance(final_producer, NLGProducer):
            # produce utterance with instant utterance producer
            self.log.info(f"Found NLGProducer {final_producer.__class__.__name__} for xmit {xmit}")
            utterance, _ = await final_producer.produce(nlg_request)
            await self.flow_handler.add_xmits(self.prepare_new_xmits(utterance, xmit))
        else:
            # start utterance production by letting the producer send instructions to other modules (DelayedProducer)
            send_data, _ = await final_producer.prepare(nlg_request)
            send_data = send_data if isinstance(send_data, list) else [send_data]
            for tier_name, _xmit in send_data:
                await self.timeboard.tiers[tier_name].add_event(_xmit)
            self.current_delayed_producer.append((final_producer, xmit))

    async def handle_new_event(self, tier: Tier, event: Event):
        # events for delayed producers
        finished_producers_idx = []
        for idx, (delayed_producer, nlg_xmit) in enumerate(self.current_delayed_producer):
            tier_names = {t.tier_name: k for t, k in delayed_producer.get_tiers(self.flow_handler.flexdiam_config, self.observer_config)}
            if tier.name in tier_names and ObservationType.OBSERVE in tier_names[tier.name]:
                # let delayed producers provide their outsourced utterance result
                finished, utterance, _ = await delayed_producer.handle_event(tier, event, nlg_xmit, self.timeboard)
                if finished:
                    finished_producers_idx.append(idx)
                    await self.flow_handler.add_xmits(self.prepare_new_xmits(utterance, nlg_xmit))
        for idx in reversed(finished_producers_idx):
            finished_producers_idx.pop(idx)

# specify this in the `further_observer_requirements`-method of an issue that you use. (Or the flow handler directly)
nlg_observer_req = ObserverReq(observer_class=NLGObserver, observer_config={})


