from aaambos.core.module.feature import Feature, FeatureNecessity

from simple_flexdiam.modules.core.definitions import NluResults, AsrResultState
from simple_flexdiam.modules.core.features import TierReq, ObserverReq
from simple_flexdiam.modules.core.observer import Observer
from simple_flexdiam.modules.core.tier import ObservationType, IntervalEvent, Tier, Event
from simple_flexdiam.modules.simple_flexdiam_module import SimpleFlexdiamConfig
from simple_flexdiam.modules.std.tiers.basic import NLUTier, UserWordsTier


ENTER_HYPOTHESES = "enter_hypotheses"
NLU_PARSE_ENTRY_POINT = "NluParseEntryPoint"


class NLUEntryPointObserver(Observer):
    """Observe the NLUTier to trigger the `NLU_PARSE_ENTRY_POINT` on the floor handler.
    Currently, it ignores partial/hypotheses and only passes the final results.
    """
    enter_hypotheses: bool = True

    @classmethod
    def provides_features(cls, config: SimpleFlexdiamConfig, observer_config: dict = ...) -> dict[
        Feature.name, tuple[Feature, FeatureNecessity]]:
        return {}

    @classmethod
    def requires_features(cls, config: SimpleFlexdiamConfig, observer_config: dict = ...) -> dict[
        Feature.name, tuple[Feature, FeatureNecessity]]:
        return {}

    @classmethod
    def get_tiers(cls, config: SimpleFlexdiamConfig, observer_config: dict) -> dict[
        TierReq, ObservationType | list[ObservationType]]:
        return {NLUTier: ObservationType.OBSERVE}

    async def initialize(self):
        await super().initialize()
        if ENTER_HYPOTHESES in self.observer_config:
            self.enter_hypotheses = self.observer_config[ENTER_HYPOTHESES]

    async def handle_new_event(self, tier: Tier, event: Event):
        data: NluResults = event.data
        if tier.name == NLUTier.tier_name:
            if data.state == AsrResultState.HYPOTHESIS and not self.enter_hypotheses:
                return
            # TODO maybe add only timeboard copy / the data?
            await self.flow_handler.process_entry_point(NLU_PARSE_ENTRY_POINT, data, self.timeboard)


nlu_entry_point_observer_req = ObserverReq(observer_class=NLUEntryPointObserver, observer_config={ENTER_HYPOTHESES: True})
