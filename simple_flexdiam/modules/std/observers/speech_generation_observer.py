from __future__ import annotations

from collections import deque
from datetime import datetime, timedelta
from typing import TYPE_CHECKING

from aaambos.core.module.feature import Feature, FeatureNecessity

from simple_flexdiam.modules.core.definitions import SpeechGenerationControl, SpeechGenerationStatus
from simple_flexdiam.modules.core.features import TierReq, ObserverReq
from simple_flexdiam.modules.core.observer import Observer
from simple_flexdiam.modules.core.tier import ObservationType, PointEvent, Tier, IntervalEvent, Event
from simple_flexdiam.modules.simple_flexdiam_module import SimpleFlexdiamConfig
from simple_flexdiam.modules.std.observers.floor_observer import FloorState
from simple_flexdiam.modules.std.tiers.basic import YieldTier, FloorTier, SpeechGenerationTier, PromptTier

if TYPE_CHECKING:
    from simple_flexdiam.modules.std.xmits.speech_generation_xmit import SpeechGenerationXMit


class SpeechGenerationObserver(Observer):
    """Receive SpeechGeneration XMits from the flow handler and puts them on the `SpeechGenerationTier` based on `YieldTier` and `FloorTier`.
    So that it does not generate multiple utterances at once, does not interrupt the user, and does not touch the yielded floor.
    """
    generation_buffer = None
    current_generate = None
    @classmethod
    def provides_features(cls, config: SimpleFlexdiamConfig, observer_config: dict = ...) -> dict[
        Feature.name, tuple[Feature, FeatureNecessity]]:
        return {}

    @classmethod
    def requires_features(cls, config: SimpleFlexdiamConfig, observer_config: dict = ...) -> dict[
        Feature.name, tuple[Feature, FeatureNecessity]]:
        return {}


    @classmethod
    def get_tiers(cls, config: SimpleFlexdiamConfig, observer_config: dict) -> dict[
        TierReq, ObservationType | list[ObservationType]]:
        return {YieldTier: [ObservationType.WRITE, ObservationType.READ], SpeechGenerationTier: [ObservationType.WRITE, ObservationType.OBSERVE], FloorTier: ObservationType.READ, PromptTier: ObservationType.WRITE}

    async def initialize(self):
        await super().initialize()
        self.generation_buffer = deque()

    async def step(self):
        if self.generation_buffer and self.can_generate():
            speech_gen = self.generation_buffer.popleft()
            self.log.trace(f"Start try generating {speech_gen[0].text!r}")
            await self.start_speech_generation(*speech_gen, prioritize=True)

    async def add_xmit(self, xmit: SpeechGenerationXMit):
        await self.start_speech_generation(speech_generation=xmit.speech_generation, yield_time=xmit.floor_yield)

    async def start_speech_generation(self, speech_generation: SpeechGenerationControl, yield_time: timedelta, prioritize: bool=False):
        if self.can_generate():
            self.log.info(f"Generate {speech_generation.text!r}")
            now = datetime.now()
            event = PointEvent(data=speech_generation, time_point=now)
            speech_generation.time = now
            self.current_generate = (speech_generation, yield_time)
            await self.timeboard.add_event(tier_name=SpeechGenerationTier.tier_name, event=event)
        elif prioritize:
            self.log.trace(f"Can not generate {speech_generation.text!r} with priority")
            self.generation_buffer.appendleft((speech_generation, yield_time))
        else:
            self.log.trace(f"Can not generate {speech_generation.text!r}")
            self.generation_buffer.append((speech_generation, yield_time))

    def can_generate(self):
        if self.current_generate:
            # self.log.info(f"Can not generate because of current generation {self.current_generate[0].text}.")
            return False
        last_yield = self.timeboard.tiers[YieldTier.tier_name].get_last_event()
        no_yield = last_yield is None or (last_yield.end_point is not None and last_yield.end_point < datetime.now())
        if no_yield:
            current_floor = self.timeboard.tiers[FloorTier.tier_name].get_last_event()
            if current_floor.data == FloorState.SILENCE:
                return True
            # self.log.info(f"Can not generate because floor is not Silence")
        else:
            ...
            # self.log.info(f"Can not generate because floor is yielded")
        return False

    async def handle_new_event(self, tier: Tier, event: Event):
        data: SpeechGenerationStatus = event.data
        if tier.name == SpeechGenerationTier.tier_name and isinstance(data, SpeechGenerationStatus):
            if data.status.is_termination_status():
                if self.current_generate:
                    if self.current_generate[1]:
                        yield_event = IntervalEvent(start_point=datetime.now(), end_point=datetime.now() + self.current_generate[1], data="yielded", short_view="yielded")
                        await self.timeboard.tiers[YieldTier.tier_name].add_event(yield_event)
                    self.current_generate = None

    def cancel_all_buffered_generation(self):
        self.generation_buffer = deque()

    async def mention_prompt_request(self):
        prompt_event = PointEvent(data="prompt", short_view="PromptRequest", time_point=datetime.now())
        await self.timeboard.tiers[PromptTier.tier_name].add_event(prompt_event)


speech_generation_observer_req = ObserverReq(observer_class=SpeechGenerationObserver, observer_config={})
