import datetime

from aaambos.core.module.feature import Feature, FeatureNecessity

from simple_flexdiam.modules.core.definitions import NluResults, AsrResults, AsrResultState
from simple_flexdiam.modules.core.features import TierReq, ObserverReq
from simple_flexdiam.modules.core.observer import Observer
from simple_flexdiam.modules.core.tier import ObservationType, Tier, IntervalEvent, Event
from simple_flexdiam.modules.simple_flexdiam_module import SimpleFlexdiamConfig
from simple_flexdiam.modules.std.tiers.basic import UserWordsTier, NLUTier, ASRTier, YieldTier


class UserWordsObserver(Observer):
    """Observe the ASRTier based on that content create IntervalEvents that represent the user words on the UserWordsTier.
     Further, it updates the YieldTier to shorten floor yields if the user already answered."""
    last_user_words = None

    @classmethod
    def provides_features(cls, config: SimpleFlexdiamConfig, observer_config: dict = ...) -> dict[
        Feature.name, tuple[Feature, FeatureNecessity]]:
        return {}

    @classmethod
    def requires_features(cls, config: SimpleFlexdiamConfig, observer_config: dict = ...) -> dict[
        Feature.name, tuple[Feature, FeatureNecessity]]:
        return {}

    @classmethod
    def get_tiers(cls, config: SimpleFlexdiamConfig, observer_config: dict) -> dict[TierReq, ObservationType | list[ObservationType]]:
        return {ASRTier: ObservationType.OBSERVE, UserWordsTier: ObservationType.WRITE, YieldTier: [ObservationType.READ, ObservationType.WRITE]}

    async def handle_new_event(self, tier: Tier, event: Event):
        data: AsrResults = event.data
        if tier.name == ASRTier.tier_name:
            # TODO check same ids
            if self.last_user_words is None:
                self.last_user_words = IntervalEvent(start_point=data.time, data={"asr": data}, short_view=data.best)
                if data.state == AsrResultState.FINAL:
                    self.last_user_words.end_point = data.time
                    self.last_user_words.start_point = data.time + datetime.timedelta(seconds=-1)
                    await self.update_last_yield(data.time)
                    self.log.info(f"New final user utterance received: {data.best!r}")

                await self.timeboard.add_event(UserWordsTier.tier_name, self.last_user_words)
            else:
                if data.state == AsrResultState.FINAL:
                    self.last_user_words.end_point = data.time
                    await self.update_last_yield(data.time)
                    self.log.info(f"New final user utterance received: {data.best!r}")
                self.last_user_words.data.update({"asr": data})
                self.last_user_words.short_view = data.best
                await self.timeboard.update_event(UserWordsTier.tier_name, self.last_user_words)
            if data.state == AsrResultState.FINAL:
                self.last_user_words = None

    async def update_last_yield(self, new_potential_endpoint: datetime.datetime):
        last_yield = self.timeboard.tiers[YieldTier.tier_name].get_last_event()
        no_yield = last_yield is None or (
                last_yield.end_point is not None and last_yield.end_point < new_potential_endpoint)
        if not no_yield:
            last_yield.end_point = new_potential_endpoint
            await self.timeboard.update_event(YieldTier.tier_name, last_yield)


user_words_observer_req = ObserverReq(observer_class=UserWordsObserver, observer_config={})
