from simple_flexdiam.modules.core.definitions import AsrResultsComFeatureIn, NluResultsComFeatureIn, ASR_RESULTS, \
    NLU_RESULTS, SpeechGenerationControlComFeatureOut, SpeechGenerationStatusComFeatureIn, AsrResultsComFeatureOut, \
    NluResultsComFeatureOut, SpeechGenerationStatusComFeatureOut, SpeechGenerationControlComFeatureIn
from simple_flexdiam.modules.core.features import TierReq


ASRTier = TierReq(tier_name=ASR_RESULTS, com_in_features=[AsrResultsComFeatureIn], com_out_features=[], additional_requirements=[AsrResultsComFeatureOut])
NLUTier = TierReq(tier_name=NLU_RESULTS, com_in_features=[NluResultsComFeatureIn], com_out_features=[], additional_requirements=[NluResultsComFeatureOut])

USER_WORDS = "UserWords"
UserWordsTier = TierReq(tier_name=USER_WORDS, com_in_features=[], com_out_features=[])

FLOOR_TIER = "Floor"
FloorTier = TierReq(tier_name=FLOOR_TIER, com_in_features=[], com_out_features=[])

YIELDED_STRUCTURE = "Yield"
YieldTier = TierReq(tier_name=YIELDED_STRUCTURE, com_in_features=[], com_out_features=[])

PROMPT_TIER = "Prompt"
PromptTier = TierReq(tier_name=PROMPT_TIER, com_in_features=[], com_out_features=[])

AGENT_WORDS = "AgentWords"
AgentWordsTier = TierReq(tier_name=AGENT_WORDS, com_in_features=[], com_out_features=[])

SPEECH_GENERATION = "SpeechGeneration"
SpeechGenerationTier = TierReq(tier_name=SPEECH_GENERATION, com_in_features=[SpeechGenerationStatusComFeatureIn], com_out_features=[SpeechGenerationControlComFeatureOut], additional_requirements=[SpeechGenerationStatusComFeatureOut, SpeechGenerationControlComFeatureIn])

NLG = "NLG"
NLGTier = TierReq(tier_name=NLG, com_in_features=[], com_out_features=[])
