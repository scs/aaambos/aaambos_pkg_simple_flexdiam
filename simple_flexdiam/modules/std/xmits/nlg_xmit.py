from datetime import timedelta
from typing import Type

from attrs import define, field

from simple_flexdiam.modules.core.nlg import NLGRequest
from simple_flexdiam.modules.core.observer import Observer
from simple_flexdiam.modules.issue_flow_handler.xmit import XMit
from simple_flexdiam.modules.std.observers.natural_language_generation_observer import NLGObserver


@define
class NLGXMit(XMit):
    nlg_request: NLGRequest
    """the nlg behavior that can produce a utterance"""
    floor_yield: float = 0.0
    """how long the agent should say nothing, so that the user can answer in seconds."""

    @classmethod
    def get_observer_class(cls) -> Type[Observer]:
        return NLGObserver
