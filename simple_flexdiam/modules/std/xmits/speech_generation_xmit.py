from datetime import timedelta
from typing import Type

from attrs import define, field

from simple_flexdiam.modules.core.definitions import SpeechGenerationControl
from simple_flexdiam.modules.core.observer import Observer
from simple_flexdiam.modules.issue_flow_handler.xmit import XMit
from simple_flexdiam.modules.std.observers.speech_generation_observer import SpeechGenerationObserver


@define(kw_only=True)
class SpeechGenerationXMit(XMit):
    speech_generation: SpeechGenerationControl
    """the control message that will be published"""
    floor_yield: timedelta = field(factory=lambda: timedelta(seconds=1))
    """how long the agent should say nothing, so that the user can answer."""

    @classmethod
    def get_observer_class(cls) -> Type[Observer]:
        return SpeechGenerationObserver
