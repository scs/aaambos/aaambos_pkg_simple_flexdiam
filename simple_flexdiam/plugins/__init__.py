"""
Simple Flexdiam extends the [`create`-command](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos/aaambos/core/commands/create.html#CreateYY) in _aaambos_.

## Issues
The bash command
```bash
aaambos create issue -n MyNewIssue
```
creates a new python file `my_new_issue.py` containing a `MyNewIssue` class based on the ExampleIssue in the aaambos pkg cookiecutter template in the `flexdiam_pkg` branch.
All names are replaced accordingly.

## Observers
The bash command
```bash
aaambos create observer -n MyNewObserver
```
creates a new python file `my_new_observer.py` containing a `MyNewObserver` class based on the ExmpleObserver in the aaambos pkg cookiecutter template in the `flexdiam_pkg` branch.
All names are replaced accordingly.

## Tiers
The bash command
```bash
aaambos create tier -n MyNewTier
```
creates a new python file `my_new_tier.py` containing a `MyNewTier` class based on the ExampleTier in the aaambos pkg cookiecutter template in the `flexdiam_pkg` branch.
All names are replaced accordingly.

## XMits
The bash command
```bash
aaambos create xmit -n MyNewXMit
```
creates a new python file `my_new_x_mit.py` containing a `MyNewXMit` class based on the ExampleXMit in the aaambos pkg cookiecutter template in the `flexdiam_pkg` branch.
All names are replaced accordingly.


"""