from loguru import logger

from aaambos.core.commands.create import Create, create_file_from_url


def issue(create: Create):
    """Entry point for creating an issue with the aaambos `create` command."""
    create_file_from_url(
        url="https://gitlab.ub.uni-bielefeld.de/api/v4/projects/6430/repository/files/{{cookiecutter.repo_name}}%2f{{cookiecutter.__package_name}}%2fissues%2fexample_issue.py/raw?ref=flexdiam_pkg",
        snake_case="example_issue",
        camel_case="ExampleIssue",
        kind="issue",
        create=create,
    )

def observer(create: Create):
    """Entry point for creating an observer with the aaambos `create` command."""
    create_file_from_url(
        url="https://gitlab.ub.uni-bielefeld.de/api/v4/projects/6430/repository/files/{{cookiecutter.repo_name}}%2f{{cookiecutter.__package_name}}%2fobservers%2fexample_observer.py/raw?ref=flexdiam_pkg",
        snake_case="example_observer",
        camel_case="Example_observer",
        kind="observer",
        create=create,
    )


def tier(create: Create):
    """Entry point for creating a tier with the aaambos `create` command."""
    create_file_from_url(
        url="https://gitlab.ub.uni-bielefeld.de/api/v4/projects/6430/repository/files/{{cookiecutter.repo_name}}%2f{{cookiecutter.__package_name}}%2ftiers%2fexample_tier.py/raw?ref=flexdiam_pkg",
        snake_case="example_tier",
        camel_case="ExampleTier",
        kind="tier",
        create=create,
    )

def xmit(create: Create):
    """Entry point for creating an xmit with the aaambos `create` command."""
    create_file_from_url(
        url="https://gitlab.ub.uni-bielefeld.de/api/v4/projects/6430/repository/files/{{cookiecutter.repo_name}}%2f{{cookiecutter.__package_name}}%2fxmits%2fexample_xmit.py/raw?ref=flexdiam_pkg",
        snake_case="example_xmit",
        camel_case="ExampleXMit",
        kind="xmit",
        create=create,
    )

def entry_point_finder(create: Create):
    """Future entry point to create an entry point finder."""
    ...


def flow_handler(create: Create):
    """Future entry point to create flow handler."""
    ...
