

simple_module_config = """\
module_info: !name:simple_flexdiam.modules.simple_flexdiam_module.SimpleFlexdiam
observer_config:
  NLGObserver:
    templates: # you can also specify a file
      greeting:
        - "Hallo!"
        - "Hey!"
        - "Guten Tag!"
        - "Huhu!"
        - "Hallo {name}!"
        - "Hey {name}!"
flow_handler:
  flow_handler_class: !name:simple_flexdiam.modules.issue_flow_handler.issue_flow_handler.IssueFlowHandler
  root_issue: !name:simple_flexdiam.modules.std.issues.root_issue.RootIssue
  shadow_root_issues:
    - !name:simple_flexdiam.modules.std.issues.greet_issue.GreetIssue
    - !name:simple_flexdiam.modules.std.issues.how_are_you_issue.HowAreYouIssue
    - !name:simple_flexdiam.modules.std.issues.repeat_issue.RepeatIssue
  # entry_point_finder: !name:simple_flexdiam.modules.issue_flow_handler.selective_entry_point_finder.SelectiveEntryPointFinder
  entry_point_finder: !name:simple_flexdiam.modules.issue_flow_handler.simple_entry_point_finder.SimpleEntryPointFinder
gui: False
"""

selective_module_config = """\
module_info: !name:simple_flexdiam.modules.simple_flexdiam_module.SimpleFlexdiam
observer_config:
  NLGObserver:
    templates: # you can also specify a file
      greeting:
        - "Hallo!"
        - "Hey!"
        - "Guten Tag!"
        - "Huhu!"
        - "Hallo {name}!"
        - "Hey {name}!"
flow_handler:
  flow_handler_class: !name:simple_flexdiam.modules.issue_flow_handler.issue_flow_handler.IssueFlowHandler
  root_issue: !name:simple_flexdiam.modules.std.issues.root_issue.RootIssue
  shadow_root_issues:
    - !name:simple_flexdiam.modules.std.issues.greet_issue.GreetIssue
    - !name:simple_flexdiam.modules.std.issues.how_are_you_issue.HowAreYouIssue
    - !name:simple_flexdiam.modules.std.issues.repeat_issue.RepeatIssue
  entry_point_finder: !name:simple_flexdiam.modules.issue_flow_handler.selective_entry_point_finder.SelectiveEntryPointFinder
  # entry_point_finder: !name:simple_flexdiam.modules.issue_flow_handler.simple_entry_point_finder.SimpleEntryPointFinder
gui: False
"""