from datetime import datetime
from typing import Any

import pytest
from aaambos.core.communication.topic import Topic
from aaambos.core.execution.standalone import StandaloneTest, StandaloneModuleTestExtension, \
    define_test_standalone_feature, EndTestException

from simple_flexdiam.modules.core.definitions import SPEECH_GENERATION_CONTROL, SpeechGenerationControl, ASR_RESULTS, \
    AsrResults, AsrResultState, NluResults, NLU_RESULTS, SpeechGenerationStatus, Status, SPEECH_GENERATION_STATUS
from simple_flexdiam.modules.simple_flexdiam_module import SimpleFlexdiam
from simple_flexdiam.modules.std.issues.greet_issue import GreetIssue
from simple_flexdiam.modules.std.tiers.basic import UserWordsTier, AgentWordsTier
from simple_flexdiam.tests import simple_module_config, selective_module_config


class BasicFlexdiamLoopTest(StandaloneModuleTestExtension):
    """Test setup and issue creation from shadow via nlu_entry_point. And check SpeechGeneration."""
    name = "BasicFlexdiamLoopTest"
    module: SimpleFlexdiam
    
    async def after_module_initialize(self, module):
        self.register_notification_for_send(self.receive_speech_generation, topic_name=SPEECH_GENERATION_CONTROL)
        self.t_start = datetime.now()
        self.step_counter = 0
        self.status = None
        assert len(self.module.flow_handler.root_issue.children) == 3
        assert all([c.state.is_shadow() for c in self.module.flow_handler.root_issue.children])
    
    async def receive_speech_generation(self, topic: Topic, msg: Any):
        assert isinstance(msg, SpeechGenerationControl)
        assert msg.text in ["Hallo!","Hey!","Guten Tag!", "Huhu!"]  # should be "Hallo!" because of seed 42 in Fill
        
        generation_status = SpeechGenerationStatus(id=msg.id, status=Status.running)
        await self.send_to_module(SPEECH_GENERATION_STATUS, generation_status)
        self.status = generation_status

    async def step(self):
        self.step_counter += 1
        
        if self.step_counter == 1:
            # send "hey!" to flexdiam
            asr_result = AsrResults(state=AsrResultState.FINAL, source="test_extension", hypotheses=["Hey!", "Hello!", "Hi"], confidences=[0.8, 0.6, 0.1], best="Hey!", best_confidence=0.8, utterance_id="test_extension_1")
            await self.send_to_module(ASR_RESULTS, asr_result)
            asr_tier_event = self.module.timeboard.tiers[UserWordsTier.tier_name].get_last_event()
            assert asr_tier_event.data["asr"] == asr_result
            nlu_result = NluResults(state=AsrResultState.FINAL, intention="greet", utterance="Hey!", entities=[], tokens=[], noun_phrases=[], utterance_id="test_extension_1")
            await self.send_to_module(NLU_RESULTS, nlu_result)
            
            # expect shadow GreetIssue was triggered and created new child
            assert len(self.module.flow_handler.root_issue.children) == 4
            assert isinstance(self.module.flow_handler.root_issue.children[-1], GreetIssue)
            assert self.module.flow_handler.root_issue.children[-1].state.is_terminated()
            assert self.module.flow_handler.root_issue.children[0].locals is not self.module.flow_handler.root_issue.children[-1].locals
            assert self.module.flow_handler.root_issue.children[0].locals != self.module.flow_handler.root_issue.children[-1].locals
        if self.step_counter == 2:
            generation_status = SpeechGenerationStatus(id=self.status.id, status=Status.finished)
            await self.send_to_module(SPEECH_GENERATION_STATUS, generation_status)
            assert self.module.timeboard.tiers[AgentWordsTier.tier_name].get_last_event().data[
                       "status"] == generation_status
        if self.step_counter == 3:
            # event has end_point set
            assert self.module.timeboard.tiers[AgentWordsTier.tier_name].get_last_event().end_point
            
        if self.step_counter == 5:
            raise EndTestException("Finished Test")
            
@pytest.mark.timeout(2)
def test_flexdiam_basic_loop_simple():
    standalone_flexdiam = StandaloneTest(
        module_config=simple_module_config,
        run_config_plus={},
        module_name="standalone_flexdiam"
    )
    standalone_flexdiam.setup(define_test_standalone_feature(BasicFlexdiamLoopTest))
    standalone_flexdiam.run()


@pytest.mark.timeout(2)
def test_flexdiam_basic_loop_selective():
    standalone_flexdiam = StandaloneTest(
        module_config=selective_module_config,
        run_config_plus={},
        module_name="standalone_flexdiam"
    )
    standalone_flexdiam.setup(define_test_standalone_feature(BasicFlexdiamLoopTest))
    standalone_flexdiam.run()
