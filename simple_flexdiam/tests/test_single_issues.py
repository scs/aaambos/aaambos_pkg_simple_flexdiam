import uuid
from datetime import datetime
from pprint import pprint

from simple_flexdiam.modules.core.definitions import NluResults, AsrResultState
from simple_flexdiam.modules.issue_flow_handler.issue import IssueOutput
from simple_flexdiam.modules.issue_flow_handler.issue_flow_handler import Context
from simple_flexdiam.modules.simple_flexdiam_module import SimpleFlexdiamConfig, SimpleFlexdiam
from simple_flexdiam.modules.std.issues.greet_issue import GreetIssue
from simple_flexdiam.modules.std.issues.how_are_you_issue import HowAreYouIssue
from simple_flexdiam.modules.std.issues.root_issue import RootIssue
from simple_flexdiam.modules.std.issues.yes_no_question_issue import YNQIssue
from simple_flexdiam.modules.std.nlg.fill import Fill
from simple_flexdiam.modules.std.nlg.say import Say
from simple_flexdiam.modules.std.observers.nlu_entry_point_observer import NLU_PARSE_ENTRY_POINT
from simple_flexdiam.modules.std.xmits.nlg_xmit import NLGXMit

root = RootIssue(abstract_name="root", parent=None, config={})

flexdiam_config = SimpleFlexdiamConfig(name="flexdiam", module_info=SimpleFlexdiam, flow_handler=None)

def show_issue_class(issue_class):
    print(f"Debug Issue: {issue_class.__name__}")
    print("Potential children classes")
    pprint(issue_class.potential_children_classes(flexdiam_config))

    print("Potential xmits classes")
    pprint(issue_class.potential_xmit_classes())

    print("Init shadow children")
    pprint(issue_class.get_init_shadow_children(flexdiam_config))

    print("Entry Point of Interest")
    pprint(issue_class.entry_points_of_interest())



def show_issue_output(output):
    print(f"Output:")
    print(f"{output.has_xmits()=}")
    print(output.get_xmits())
    print(f"{output.has_assigned_children()=}")
    print(f"{output.get_assigned_children()}")

def trigger_entry_point(issue, ep, ep_data, context):
    print(f"EP {issue=} with {ep=} {ep_data=}")
    can_handle = issue.can_handle_entry_point(ep, ep_data, context)
    print(f"EP can_handle_ep {can_handle=} {ep=} {ep_data=}")
    output = None
    if can_handle:
        output = IssueOutput(issue)
        issue.handle_entry_point(ep, ep_data, context, output)
        show_issue_output(output)
    print(f"Issue state {issue.state}")
    return output

def trigger_prompt_request(issue, context):
    print(f"Prompt RQ {issue=}")
    output = IssueOutput(issue)
    issue.handle_prompt_request(context, output)
    show_issue_output(output)
    print(f"Issue state {issue.state}")
    return output

def trigger_child_closed(issue, child, context):
    print(f"Child closed {child=} for  {issue=}")
    output = IssueOutput(issue)
    issue.handle_child_closed(child, context, output)
    show_issue_output(output)
    print(f"Issue state {issue.state}")
    return output


def test_greet_issue():
    debug_issue_class = GreetIssue
    show_issue_class(debug_issue_class)
    debug_issue = debug_issue_class(abstract_name="test_issue", parent=root, config={}, as_shadow=True)
    debug_issue.initialize()

    context = Context()

    output = trigger_entry_point(debug_issue, NLU_PARSE_ENTRY_POINT, NluResults(
        state=AsrResultState.FINAL,
        intention="greet",
        utterance="hallo",
        entities=[],
        tokens=[],
        noun_phrases=[],
        utterance_id=uuid.uuid4().hex,
        time=datetime.now(),
    ), context)

    assert output.has_xmits() and isinstance(output.get_xmits()[0], NLGXMit) and isinstance(output.get_xmits()[0].nlg_request, Fill) and output.get_xmits()[0].nlg_request.template_ref == "greeting"
    # assert output.has_xmits() and isinstance(output.get_xmits()[0], NLGXMit) and isinstance(output.get_xmits()[0].nlg_request, Say) and output.get_xmits()[0].nlg_request.utterance == "hallo"

    debug_issue = debug_issue_class(abstract_name="test_issue", parent=root, config={}, as_shadow=True)
    debug_issue.initialize()
    output = trigger_entry_point(debug_issue, NLU_PARSE_ENTRY_POINT, NluResults(
        state=AsrResultState.FINAL,
        intention="no_greet",
        utterance="Wie geht es dir",
        entities=[],
        tokens=[],
        noun_phrases=[],
        utterance_id=uuid.uuid4().hex,
        time=datetime.now(),
    ), context)

    assert output is None

    print("Done\n")

def test_how_are_you_issue():
    """Create How are you issue. Request howareyou. Issue should ask how you feel. Answering notsowell triggers a child issue and this asks if you need help. If you affirm or deny the child issue terminates and the howareaou issue suggests to talk to a friend."""
    debug_issue_class = HowAreYouIssue
    show_issue_class(debug_issue_class)
    debug_issue = debug_issue_class(abstract_name="test_issue", parent=root, config={}, as_shadow=True)
    debug_issue.initialize()
    context = Context()

    # hask how the agent feels
    output = trigger_entry_point(debug_issue, NLU_PARSE_ENTRY_POINT, NluResults(
        state=AsrResultState.FINAL,
        intention="howareyou",
        utterance="Wie geht es dir",
        entities=[],
        tokens=[],
        noun_phrases=[],
        utterance_id=uuid.uuid4().hex,
        time=datetime.now(),
    ), context)

    # should answer good, and request how you feel
    assert output.has_xmits() and isinstance(output.get_xmits()[0], NLGXMit) and isinstance(output.get_xmits()[0].nlg_request, Say) and output.get_xmits()[0].nlg_request.utterance == "Wenn ich dich sehe, geht es mir eigentlich immer gut! Und wie geht es dir?"

    # say not so well
    output = trigger_entry_point(debug_issue, NLU_PARSE_ENTRY_POINT, NluResults(
        state=AsrResultState.FINAL,
        intention="feelingnotsowell",
        utterance="schlecht",
        entities=[],
        tokens=[],
        noun_phrases=[],
        utterance_id=uuid.uuid4().hex,
        time=datetime.now(),
    ), context)

    # agent: that not good
    assert output.has_xmits() and isinstance(output.get_xmits()[0], NLGXMit) and isinstance(
        output.get_xmits()[0].nlg_request, Say) and output.get_xmits()[
               0].nlg_request.utterance == "Das klingt aber nicht so gut."

    # should have a child issue
    assert output.has_assigned_children() and output.get_assigned_children()[0][0] is YNQIssue
    # init child issue
    child_data = output.get_assigned_children()[0]
    show_issue_class(child_data[0])
    child_issue = child_data[0](abstract_name=child_data[1], parent=debug_issue, config=child_data[2])
    child_issue.initialize()
    # trigger prompt request for new child
    output = trigger_prompt_request(child_issue, context)
    # asks if you want to talk about
    assert output.has_xmits() and isinstance(output.get_xmits()[0], NLGXMit) and isinstance(
        output.get_xmits()[0].nlg_request, Say) and output.get_xmits()[
               0].nlg_request.utterance == "Willst du darueber reden?"

    # yes
    output = trigger_entry_point(child_issue, NLU_PARSE_ENTRY_POINT, NluResults(
        state=AsrResultState.FINAL,
        intention="affirm",
        utterance="ja",
        entities=[],
        tokens=[],
        noun_phrases=[],
        utterance_id=uuid.uuid4().hex,
        time=datetime.now(),
    ), context)

    assert not child_issue.is_active()
    # check closed on debug
    output = trigger_child_closed(debug_issue, child_issue, context)
    assert output.has_xmits() and isinstance(output.get_xmits()[0], NLGXMit) and isinstance(
        output.get_xmits()[0].nlg_request, Say) and output.get_xmits()[
               0].nlg_request.utterance == "Vielleicht hilft es dir wenn du einen guten Freund anrufst. Über Probleme zu reden, ist ein guter Weg sie aus der Welt zu schaffen."

    assert not debug_issue.is_active()
    print("Done\n")
    